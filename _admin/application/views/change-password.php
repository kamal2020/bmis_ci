<?php $this->load->view('include/header'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Change Password</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          <div class="box box-widget study-material">
            <div class="box-header">
              <h2>Change your Password</h2>          
            </div>
            <div class="box-content">
            <?php if($this->session->flashdata('flash')){ ?>
            <div class="col-md-12 text-center">
              <span class="col-md-11 alert alert-warning"><?=$this->session->flashdata('flash')?></span>
            </div>
            <?php } ?>
              <form role="form" action="<?=DOMAIN.'profile/replace_password'?>" id="changePassword" method="post">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Old Password</label>
                      <input class="form-control" placeholder="Old Password" name="oPassword" type="password">
                    </div>
                    <div class="form-group">
                      <label>New Password</label>
                      <input class="form-control" id="password" placeholder="New Password" name="nPassword" type="password">
                    </div>
                    <div class="form-group">
                      <label>Confirm Password</label>
                      <input class="form-control" placeholder="Confirm Password" name="rnPassword" type="password">
                    </div>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-orange">Submit</button>
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
<script>
  $('#changePassword').validate({
    rules: {
    // simple rule, converted to {required:true}
    oPassword: "required",
    nPassword:{
                required:true,
              },
    rnPassword:{
                required:true,
                equalTo: "#password"
              } 
  }
  });
</script>