<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      Source Code - Source Name 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Source Code - Source Name </li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
            <h3 class="box-title">Source Code - Source Name</h3>
                        
          </div>
          
          <!-- /.box-header -->
          <div class="box-body table-responsive">
          <form role="form" method="post" action="" id="profile">
          <input type='hidden' name='id' value='<?php if($listing){ echo $listing->id; } ?>' >
           
          <div class="form-group col-md-6">
                      <label>Source Code</label>
                      <input type="text" class="form-control" name="source_code" value="<?php if($listing){ echo $listing->source_code; } ?>" placeholder="Enter source code" required>
          </div>
          <div class="form-group col-md-6">
                      <label>Source Name</label>
                      <input type="text" class="form-control" name="source_name" value="<?php if($listing){ echo $listing->source_name; } ?>" placeholder="Enter source name" required>
          </div>

          <div class="box-footer">
                  <div class="form-group col-md-12">
                  <button type="submit" name='submit' value='submit' class="btn btn-primary">Submit</button>
                  <button class="btn btn-default backLink">Go Back</button>

                </div>
          </form>
          
          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
