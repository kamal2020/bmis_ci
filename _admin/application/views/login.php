<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BMIS</title>
  <!-- Responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="favicon.ico">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=ASSETS?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/font-awesome.css">  
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/style.css">
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/skins/skin-black.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/custom.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-black login-page">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>
<!-- Wrapper -->
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="login-content">
        <!--Show header and side-bar only to logged in user-->
        <!-- Main content -->
                                                
        <div class="site-login">
          <div class="login-logo text-center"><img src="<?=ASSETS?>dist/img/logo.png" alt="AU Finance"></div>
          <div class="text-center">
            <div class="col-lg-3 login-block" >
            <?=$this->session->flashdata('flash')?>
              <form action="<?=DOMAIN?>login/verifyLogin" method="post" id="login-form" role="form">
                <input type="hidden" name="" >
                <span class="input input--madoka" id="login-email">
                  <input type="email" required="" id="input-31" class="input__field input__field--madoka" name="email" autofocus>                
                  <label class="input__label input__label--madoka" for="input-31">
                    <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                      <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                    </svg>
                    <span class="input__label-content input__label-content--madoka">Email</span>
                  </label>
                </span>
                <span class="input input--madoka" id="login-password">

                  <input type="password" required="" id="input-32" class="input__field input__field--madoka" name="pass">                
                  <label class="input__label input__label--madoka" for="input-32">
                    <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                      <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                    </svg>
                    <span class="input__label-content input__label-content--madoka">Password</span>
                  </label>
                </span>

              <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right login-btn" name="login-button">Login</button><!--  <a href="javascript:void(0);" class="forgot pull-left">Forgot Password <span class="fa fa-question-circle"></span></a> -->
              </div>
            </form>        
          </div>
        </div>
      </div>

</section>
</div><!-- /.content-wrapper -->
</div>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="<?=ASSETS?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=ASSETS?>bootstrap/js/bootstrap.min.js"></script>
<!-- App -->
<script src="<?=ASSETS?>dist/js/app.min.js"></script>
<script>
$(document).ready(function() {
  
  setTimeout(function(){
    $('body').addClass('loaded');
  }, 1500);
  
});
</script>
</body>
</html>
