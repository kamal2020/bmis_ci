<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      City
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">City</li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
            <h3 class="box-title">City List</h3>
            <span class="pull-right"><a href='<?=DOMAIN?>master/add_edit_city' class='btn btn-primary'>Add New City</a></span>            
          </div>
          
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-condenced">
              <thead>
              <tr>
                <th>Serial #</th>
                <th>State Name</th>
                <th>City Name</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              <?php $i=0; foreach($listing as $key=>$value) {?>
                <tr>
                
                  <td><?=++$i?></td>
                  <td><?=$value->state_name?></td>
                  <td><?=$value->city_name?></td>
                                 
                  <td> 
                    
                      <a href="<?=DOMAIN.'master/add_edit_city/'.$value->id?>" class="btn btn-warning btn-xs">Edit</a>
                     
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
