<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      Business Type
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">State</li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
            <h3 class="box-title">Business Type</h3>
                        
          </div>
          
          <!-- /.box-header -->
          <div class="box-body table-responsive">
          <form role="form" method="post" action="" id="profile">
          <input type='hidden' name='id' value='<?php if($listing){ echo $listing->id; } ?>' >
          <div class="form-group col-md-6">
                      <label>Select Verticle</label>
                      <select required name='verticle_id' class='form-control'>
                        <option value=''>Select Verticle</option>
                        <?php foreach($verticle as $raw)
                        {?>
                        <option value='<?=$raw->id?>' <?php if($listing){ if($listing->verticle_id==$raw->id){ echo 'selected'; } } ?>><?=$raw->verticle?></option>
                        <?php } ?>
                        
                      </select>
          </div>        
          <div class="form-group col-md-6">
                      <label>Business Type</label>
                      <input type="text" class="form-control" name="business_type" value="<?php if($listing){ echo $listing->business_type; } ?>" placeholder="Enter business type" required>
          </div>

          <div class="box-footer">
                  <div class="form-group col-md-12">
                  <button type="submit" name='submit' value='submit' class="btn btn-primary">Submit</button>
                  <button class="btn btn-default backLink">Go Back</button>

                </div>
          </form>

          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
