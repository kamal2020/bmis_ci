<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header Vehicle Category, Product,Brokerage Premuim,Verticle,Sourcing Category,Hpn,business_type-->
  	<section class="content-header">
    	<h1>User</h1>
      	<ol class="breadcrumb">
        	<li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        	<li class="active">View Bmis</li>
        	<li class="active"><?php if($listing){ echo $listing->id; } ?></li>
      	</ol>
    </section>

  	<section class="content">

    	<div class="row">

      		<div class="col-md-12">
        
        		<div class="box">
       
          <!-- <div class="box-header">
            <h3 class="box-title">User</h3>
                        
          </div> -->
          
          <!-- /.box-header -->
                    
          			<div class="box-body table-responsive">

                      <div>
                      <a class="btn btn-primary btn-lg" href="<?=DOMAIN.'bmis/add_edit_bmis/'.$listing->id?>">UPDATE</a>
                        <a class="btn btn-danger btn-lg" href="<?=DOMAIN.'bmis/delete_bmis/'.$listing->id?>">DELETE</a>
                      </div>
                        <hr>
          				<table class="table table-responsive table-striped">
          					<tr>
                              <td>Id</td>
                              <td>
                                <?php if($listing){ echo $listing->id; } ?>
                              </td>
                            </tr>
          					<tr>
                              <td>Receive From</td>
                      		  <td><?php if($listing){ echo $listing->receive_from; } ?></td>
          					</tr>
         					<tr>
                      			<td>Month</td>
                      			<td><?php if($listing){ echo $listing->month; } ?></td>
         					</tr>
							<tr>
                      			<td>Entry Date</td>
                      			<td><?php if($listing){ echo $listing->entry_date; } ?></td>
        					</tr>
							<tr>
                      			<td>State</td>
								<td><?php if($listing){ echo $listing->state; } ?></td>
                      			
        					</tr>
							<tr>
                      			<td>Cluster</td>
                      			<td><?php if($listing){ echo $listing->cluster; } ?></td>
        					</tr>
							<tr>
                      			<td>Location</td>
                      			<td><?php if($listing){echo $listing->location; } ?></td>
        					</tr>
							<tr>
                      			<td>Customer Name</td>
                      			<td><?php if($listing){ echo $listing->customer_name; } ?></td>
          					</tr>
          					<tr>
                      			<td>Contact No</td>
                      			<td><?php if($listing){ echo $listing->contact_no; } ?></td>
          					</tr>
          					<tr>
                      			<td>Customer Address</td>
                      			<td><?php if($listing){ echo $listing->customer_address; } ?></td>
          					</tr>
							  <tr>
                      			<td>Customer State</td>
								<td><?php if($listing){ echo $listing->customer_state; } ?></td>
                      			
          					</tr>
							<tr>
                      			<td>Customer City</td>
                      			<td><?php if($listing){ echo $listing->customer_city; } ?></td>
         					</tr>
          					<tr>
                      			<td>Make</td>
                      			<td><?php if($listing){ echo $listing->make; } ?></td>
          					</tr>
          					<tr>
                      			<td>Model</td>
                      			<td><?php if($listing){ echo $listing->model; } ?></td>
          					</tr>
         					<tr>
                      			<td>Reg No</td>
                      			<td><?php if($listing){ echo $listing->reg_no; } ?></td>
         					</tr>
							<tr>
                      			<td>Engine No</td>
                      			<td><?php if($listing){ echo $listing->engine_no; } ?></td>
          					</tr>
          					<tr>
                      			<td>Chasis No</td>
                      			<td><?php if($listing){ echo $listing->chasis_no; } ?></td>
          					</tr>
          					<tr>
                      			<td>Vehicle Category</td>
                                <td><?php if($listing){ echo $listing->vehicle_category; } ?></td>
          					</tr>
							  <tr>
                      			<td>Product</td>
                      			<td><?php if($listing){ echo $listing->product; } ?></td>
          					</tr>
         					<tr>
                      			<td>Insurance Company</td>
                      			<td><?php if($listing){ echo $listing->insurance_company; } ?></td>
         					</tr>
							<tr>
                      			<td>Company Office</td>
                      			<td><?php if($listing){ echo $listing->company_office; } ?></td>
          					</tr>
          					<tr>
                      			<td>Hpn</td>
                      			<td><?php if($listing){ echo $listing->hpn; } ?></td>
          					</tr>
          					<tr>
                      			<td>Manufacturing year</td>
                      			<td><?php if($listing){ echo $listing->manufacturing_year; } ?></td>
          					</tr>
          					<tr>
                      			<td>Issue Date</td>
                      			<td><?php if($listing){ echo $listing->issue_date; } ?></td>
          					</tr>
 							<tr>
                      			<td>Risk Start Date</td>
                      			<td><?php if($listing){ echo $listing->risk_start_date; } ?></td>
          					</tr>
          					<tr>
                      			<td>Risk End Date</td>
                      			<td><?php if($listing){ echo $listing->risk_end_date; } ?></td>
          					</tr>
          					<tr>
                      			<td>Policy Type</td>
                      			<td><?php if($listing){ echo $listing->policy_type; } ?></td>
          					</tr>
          					<tr>
                      			<td>Cover Note Number</td>
                      			<td><?php if($listing){ echo $listing->cover_note_number; } ?></td>
          					</tr>
 							<tr>
                      			<td>Policy Number</td>
                      			<td><?php if($listing){ echo $listing->policy_number; } ?></td>
          					</tr>
          					<tr>
                      			<td>Discount %</td>
                      			<td><?php if($listing){ echo $listing->disscount; } ?></td>
          					</tr>
          					<tr>
                      			<td>Current Ncb</td>
                      			<td><?php if($listing){ echo $listing->current_ncb; } ?></td>
          					</tr>
          					<tr>
                      			<td>Sum Insured</td>
                      			<td><?php if($listing){ echo $listing->sum_insured; } ?></td>
          					</tr>
 							<tr>
                      			<td>Pacakge Od</td>
                      			<td><?php if($listing){ echo $listing->pacakge_od; } ?></td>
          					</tr>
          					<tr>
                      			<td>Liability Tp</td>
                      			<td><?php if($listing){ echo $listing->liability_tp; } ?></td>
          					</tr>
							<tr>
                      			<td>Brokerage Premuim</td>
                      			<td><?php if($listing){ echo $listing->brokerage_premium; } ?></td>
          					</tr>
          					<tr>
                      			<td>Gross Premium</td>
                      			<td><?php if($listing){ echo $listing->gross_premium; } ?></td>
          					</tr>
          					<tr>
                      			<td>Service Tax</td>
                      			<td><?php if($listing){ echo $listing->service_tax; } ?></td>
          					</tr>
 							<tr>
                      			<td>Total Premium</td>
                      			<td><?php if($listing){ echo $listing->total_premium; } ?></td>
          					</tr>
          					<tr>
                      			<td>Upfront</td>
                      			<td><?php if($listing){ echo $listing->upfront; } ?></td>
          					</tr>
          					<tr>
                      			<td>Payout Percent</td>
                      			<td><?php if($listing){ echo $listing->payout_percent; } ?></td>
          					</tr>
          					<tr>
                      			<td>Payment Received Mode</td>
                      			<td><?php if($listing){ echo $listing->payment_received_mode; } ?></td>
          					</tr>
 							<tr>
                      			<td>Payment Received Mode Detail</td>
                      			<td><?php if($listing){ echo $listing->payment_received_mode_detail; } ?></td>
          					</tr>
          					<tr>
                      			<td>Payment Received Amount</td>
                      			<td><?php if($listing){ echo $listing->payment_received_amount; } ?></td>
          					</tr>
          					<tr>
                      			<td>Business Type</td>
                                  <td><?php if($listing){ echo $listing->business_type; } ?></td>
          					</tr>
							<tr>
                      			<td>Verticle</td>
                      			<td><?php if($listing){ echo $listing->verticle; } ?></td>
          					</tr>
          					<tr>
                      			<td>Source Type</td>
                                  <td><?php if($listing){ echo $listing->source_type; } ?></td>
          					</tr>
							  <tr>
                      			<td>Sourcing Category</td>
                      			<td><?php if($listing){ echo $listing->sourcing_category; } ?></td>
          					</tr>


 							<tr>
                      			<td>Source Code</td>
                      			<td><?php if($listing){ echo $listing->source_code; } ?></td>
          					</tr>
          					<tr>
                      			<td>Source Name</td>
                      			<td><?php if($listing){ echo $listing->source_name; } ?></td>
          					</tr>
          					<tr>
                      			<td>Insurance Executive Code</td>
                      			<td><?php if($listing){ echo $listing->source_code_1_insurance_executive_code; } ?></td>
          					</tr>
          					<tr>
                      			<td>Insurance Executive Name</td>
                      			<td><?php if($listing){ echo $listing->source_name_1_insurance_executive_name; } ?></td>
          					</tr>


							<tr>
                      			<td>Remark</td>
                      			<td><?php if($listing){ echo $listing->remark; } ?></td>
          					</tr>
                        </table>
							<div class="box-footer">
							<div class="form-group col-md-12">
							
							</div>

          			</div>
         
          <!-- /.box-body -->
        		</div>
      		</div>
    	</div>
  	</section>
</div>
</div>

<?php $this->load->view('include/footer'); ?>
