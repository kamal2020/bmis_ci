<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Agent</li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
            <h3 class="box-title">User</h3>
                        
          </div>
          
          <!-- /.box-header -->
          <div class="box-body table-responsive">
          <?php if($this->session->flashdata('msg')){ ?>
          <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> <?=$this->session->flashdata('msg');?>
          </div>
          <?php }?>
          <form role="form" method="post" action="" id="profile">
          
          <input type='hidden' name='id' id='id' value='<?php if($listing){ echo $listing->id; } ?>' >
          <div class="form-group col-md-6">
                      <label>Name *</label>
                      <input type="text" class="form-control" name="username" value="<?php if($listing){ echo $listing->username; } ?>" placeholder="Enter Name" required>
          </div>
          <div class="form-group col-md-6">
                      <label>Email Address *</label>
                      <input type="email" class="form-control" name="email" id="email" value="<?php if($listing){ echo $listing->email; } ?>" placeholder="Enter Email" required>
          </div>
          <div class="form-group col-md-6">
                      <label>Employee ID *</label>
                      <input type="text" class="form-control" name="unique_id" id="unique_id" value="<?php if($listing){ echo $listing->unique_id; } ?>" placeholder="Enter Employee ID" required>
                      
          </div>
          <div class="form-group col-md-6">
                      <label>Contact no</label>
                      <input type="number"  class="form-control" name="contact_no" id="contact_no" value="<?php if($listing){ echo $listing->contact_no; } ?>" placeholder="Enter contact no" required>
          </div>
          
          <div class="form-group col-md-6">
                      <label>State</label>
                      <input type="hidden" name="state" id="state" value='<?php if($listing){ echo $listing->state; } ?>'>
                      <select required name='state_id' id='state_id' class='form-control'>
                        <option value=''>Select State</option>
                        <?php foreach($state as $raw)
                        {?>
                        <option value='<?=$raw->id?>' <?php if($listing){ if($listing->state==$raw->state_name){ echo 'selected'; } } ?>><?=$raw->state_name?></option>
                        <?php } ?>
                        
                      </select>
                      
          </div>
          <div class="form-group col-md-6">
                      <label>location</label>
                      <select required name='location' id='location' class='form-control'>
                        <option value=''>Select Location</option>
                        <?php if($listing){ foreach($location as $raw)
                        {?>
                        <option value='<?=$raw->location?>' <?php if($listing){ if($listing->location==$raw->location){ echo 'selected'; } } ?>><?=$raw->location?></option>
                        <?php } } ?>
                        
                      </select>
                      
          </div>
          <div class="form-group col-md-6">
                      <label>Cluster</label>
                      
                      <select required name='cluster' id='cluster' class='form-control'>
                        <option value=''>Select Cluster</option>
                        <?php if($listing){ foreach($cluster as $raw)
                        {?>
                        <option value='<?=$raw->cluster_name?>' <?php if($listing){ if($listing->cluster==$raw->cluster_name){ echo 'selected'; } } ?>><?=$raw->cluster_name?></option>
                        <?php } } ?>
                        
                      </select>
          </div>
          <div class="form-group col-md-6">
                      <label>Password</label>
                      <input type="text" class="form-control" name="password" id="password" value="" placeholder="Enter Password" <?php if($listing==''){ echo 'required'; }?>>
          </div>
          <div class="form-group col-md-6">
                      <label>Confirm Password</label>
                      <input type="text" class="form-control" name="conf_pass" id="conf_pass" value="" placeholder="Enter Confirm Password" <?php if($listing==''){ echo 'required'; }?>>
          </div>
          <?php if($this->session->userdata('auth')['role_id']==1){ ?>
          <div class="form-group col-md-6">
                      <label>Agent Type</label>
                      
                       <select required name='role_id' id='role_id' class='form-control'>
                        <option value='1' <?php if($listing){ if($listing->role_id=='1'){ echo 'selected'; } } ?>>Admin</option>
                        <option value='2' <?php if($listing){ if($listing->role_id=='2'){ echo 'selected'; } } else { echo 'selected'; } ?>>Agent</option>
                                                
                       </select>
          </div>
          <?php } ?>
          <span id='email-error' class='error'><?=form_error('email')?></span>
          <span id='unique_id-error' class='error'><?=form_error('unique_id')?></span>
          <div class="box-footer">
                  <div class="form-group col-md-12">
                  <button type="submit" name='submit' id='submit' value='submit' class="btn btn-primary">Submit</button>
                  <button class="btn btn-default backLink">Go Back</button>
          </div>
          </form>

          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<script>
  $(function() {

    $("#submit").click(function() {
      var contact_no= $('#contact_no').val();

     if(contact_no.length<10 || contact_no.length>15)
     {
      alert('Contact number must be 10-15 Degit.');
      $('#contact_no').focus();
      return false;
     }
     if($('#conf_pass').val()!='')
     {
       if($('#conf_pass').val()!=$('#password').val())
       {
         alert('Confirm Password not match');
         return false;
       }

     }
    });

    $("#contact_no").change(function() {
      var contact_no= $('#contact_no').val();

      if(contact_no.length<10 || contact_no.length>15)
      {
        alert('Contact number must be 10-15 Degit.');
        $('#contact_no').focus();
      }
      
      
    });
    $("#email").change(function() {
      var email= $('#email').val();
      var id= $('#id').val();
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>profile/checkemail",
            data: {email:email,id:id},
            dataType: "json",
            success: function(d) {
                if (d.status == 1)
                {
                  $('#email').val('');
                  alert('This email is already exist');  
                }
                
            }
        });
    });
    $("#unique_id").change(function() {
      var unique_id= $('#unique_id').val();
      var id= $('#id').val();
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>profile/check_unique_id",
            data: {unique_id:unique_id,id:id},
            dataType: "json",
            success: function(d) {
                if (d.status == 1)
                {
                  $('#unique_id').val('');
                  alert('This unique id is already exist');  
                }
                
            }
        });
    });

    $("#state_id").change(function() {
      var state_id= $('#state_id').val();
      var thisvalue = $(this).find("option:selected").text();
      $('#state').val(thisvalue);
      //alert(thisvalue);
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>profile/get_cluster",
            data: {state_id : state_id},
            dataType: "json",
            success: function(d) {
                if ((parseInt(d.msg_status) == 0))
                {
                  var option_string_default = '';
                  var option_string = '<option value="">Please Select</option>';
                }
                else {
                    var option_string_default = '';
                    var option_string = '<option value="">Please Select</option>';
                    var option_string1 = '<option value="">Please Select</option>';
                    var selected='';
                    $.each(d.data, function(i, e) {
                        selected='';
                        //if(id2==e.id){selected='selected'; }
                        option_string += '<option '+ selected + ' ';
                        option_string += 'value="' + e.cluster_name	+ '" ';
                        option_string += '>';
                        option_string += e.cluster_name;
                        option_string += '</option>';
                    });
                    $.each(d.data2, function(i, e) {
                        selected='';
                        //if(id2==e.id){selected='selected'; }
                        option_string1 += '<option '+ selected + ' ';
                        option_string1 += 'value="' + e.location	+ '" ';
                        option_string1 += '>';
                        option_string1 += e.location;
                        option_string1 += '</option>';
                    });
                    }
                $("#cluster").html('').append(option_string_default + ' ' + option_string);
                $("#location").html('').append(option_string_default + ' ' + option_string1);
            }
        });
    });
});
  </script>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
