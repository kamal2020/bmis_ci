  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">BMIS</a>.</strong> All rights reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
  <!-- Logout Modal Box -->
  <div class="modal fade in" id="logout" style="display: none; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>                            
          <div class="modal-body">
            <div class="logout-success">
              <p>Are you sure you want to logout?</p>
              <div class="modal-btn">
                <a class="btn btn-default" data-dismiss="modal" href="javascript:void(0);">Cancel</a>
                <a class="btn btn-orange" href="<?=DOMAIN.'profile/logout'?>">OK</a>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="<?=ASSETS?>bootstrap/js/bootstrap.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=l9lhqertnupubjimisc7tjg9uqle0d28nrivmcvwee4qdf6m"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<!-- App -->
<script src="<?=ASSETS?>dist/js/app.min.js"></script>
<script src="<?=ASSETS?>dist/js/custom.js"></script>
<script src="<?=ASSETS?>dist/js/jquery.dataTables.min.js"></script>
<script src="<?=ASSETS?>dist/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=ASSETS?>dt.js">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>
$(document).ready(function(){
    $('.backLink').click(function(){
        parent.history.back();
        return false;
    });
});

  $(function () {
    $('table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    $('input').attr('autocomplete', 'off');
  });
 
 
</script>

</body>
</html>
