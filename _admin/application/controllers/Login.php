<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('LoginModel');
	}
	public function index(){
		$this->load->view('login');
	}
	public function verifyLogin(){
		if($this->input->post('email') && $this->input->post('pass')){
			$data = $this->LoginModel->checkEmail();
			if(!empty($data)){
				if(varifyPassword($_POST['pass'],$data[0]->password_hash)){
					$this->CoreModel->setSession($data);
					redirect(DOMAIN.'dashboard');
				}
				else
					$this->session->set_flashdata('flash', 'Invalid Password');
			}
			else
				$this->session->set_flashdata('flash', 'Invalid Email Address');	
		}else
			$this->session->set_flashdata('flash', 'Invalid Inputs');
		$this->load->view('login');
	}
}
