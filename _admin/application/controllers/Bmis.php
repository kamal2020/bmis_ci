<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bmis extends CI_Controller {
	function __construct(){
		parent::__construct();
		checkSession();
		// $this->load->model('LoginModel');
	}
	public function index(){
		$range = null;
		
		if ($this -> input -> post('date_range'))
		{
			$range = explode(" - ", $_POST['date_range']);
			$this -> session -> set_flashdata('dates', $_POST['date_range']);
		}
		$data['bmis'] = $this->CoreModel->get_bmis_listing_details('',$range);
		
		if ($this -> input -> post('fields'))
		{
			
			$parm = implode(",", $_POST['fields']);
		if ($this -> input -> post('date_range_export'))
		{
			$range = explode(" - ", $_POST['date_range_export']);
		}
			$bmis_data = $this -> CoreModel -> get_bmis_listing_details($parm,$range);
		}
		$this->load->view('bmis',$data);
	}
	public function get_cluster()
	{
		$state_id=$this->input->post('state_id');
		$data=$this->CoreModel->get_listing_details('cluster','',$where=array('state_id'=>$state_id));
		$d=array('status'=>1,'data'=>$data);
		echo json_encode($d);
	}
	public function get_city()
	{
		$state_id=$this->input->post('state_id');
		$data=$this->CoreModel->get_listing_details('city','',$where=array('state_id'=>$state_id));
		$d=array('status'=>1,'data'=>$data);
		echo json_encode($d);
	}

	public function add_edit_bmis($id=null){
		
		$data['listing']='';
		if($id)
		{
			$data['listing'] = $this->CoreModel->get_full_description('bmis','',$where=array('id'=>$id));
		}
		$data['state'] = $this->CoreModel->get_listing_details('state','','', 'state_name asc');
		$data['location'] = $this->CoreModel->get_listing_details('location','','', 'location asc');		
		$data['insurance_company'] = $this->CoreModel->get_listing_details('insurance_company','','', 'insurance_company asc');		
		$data['hpn'] = $this->CoreModel->get_listing_details('hpn_table','','', 'hpn asc');		
		$data['policy_type'] = $this->CoreModel->get_listing_details('policy_type','','', 'policy_type asc');		
		$data['ncb'] = $this->CoreModel->get_listing_details('ncb_tbl','','', 'ncb asc');		
		$data['payment_received_mode'] = $this->CoreModel->get_listing_details('payment_received_mode','','', 'payment_mode asc');		
		$data['business_type'] = $this->CoreModel->get_listing_details('business_type','','', 'business_type asc');		
		$data['source_type'] = $this->CoreModel->get_listing_details('source_type','','', 'source_type asc');		
		$data['insurance_exec_code'] = $this->CoreModel->get_listing_details('insurance_exec_code','','', 'insurance_exec_code asc');		
		$data['vehicle_category'] = $this->CoreModel->get_listing_details('vehicle_category','','', 'vehicle_category asc');		
		$data['source'] = $this->CoreModel->get_listing_details('source_code_source_name','','', 'id asc');		
		
		if($this->input->post('submit'))
	    {
			
		unset($_POST['submit']);
		unset($_POST['state_id']);
		unset($_POST['customer_state_id']);
		unset($_POST['vehicle_category_id']);
		unset($_POST['business_type_id']);
		unset($_POST['source_type_id']);

		$_POST['entry_date']=date('Y-m-d',strtotime($this->input->post('entry_date')));
		$_POST['issue_date']=date('Y-m-d',strtotime($this->input->post('issue_date')));
		$_POST['risk_start_date']=date('Y-m-d',strtotime($this->input->post('risk_start_date')));
		$_POST['risk_end_date']=date('Y-m-d',strtotime($this->input->post('risk_end_date')));

		$date=date('Y-m-d');
		if(date('j', strtotime($date)) === '1') {
			$new_id=str_pad(0 + 1, 6, 0, STR_PAD_LEFT);
			$_POST['unique_id']='AuIBS-'.date('m-y').'\\'.$new_id;
		 }
		 else
		 { //get last unique_id //AuIBS-07-18\000030
			$unique_id=$this->CoreModel->get_full_description('bmis','unique_id','','id desc');
			if($unique_id){
			
			$id=explode("\\",$unique_id->unique_id);
			$month_year=explode('-',$id[0]);
            if($month_year[1]==date('m') && $month_year[2]==date('y'))
			{$new_id=str_pad($id[1] + 1, 6, 0, STR_PAD_LEFT);}
			else
			{$new_id=str_pad(0 + 1, 6, 0, STR_PAD_LEFT);}
 		    }
			else
			{
				$new_id=str_pad(0 + 1, 6, 0, STR_PAD_LEFT);	
			}
			$_POST['unique_id']='AuIBS-'.date('m-y').'\\'.$new_id;
		 }

		 ///add update work/////////
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		$this->CoreModel->update_details('bmis',$_POST,$where);
		}
		else
		{
		unset($_POST['id']);
		$_POST['user_id']=$this->session->userdata('auth')['id'];
		$this->CoreModel->insert_details('bmis',$_POST);
        
		//var_dump($_POST);die;
		}
		///////////////////////////
		redirect(DOMAIN.'bmis');
	  }
	  $this->load->view('add_edit_bmis', $data);
	}

	public function delete_bmis($id=null)
	{
		if($id)
		{
			$this->db->delete('bmis',$where=array('id'=>$id));
		}
		redirect(DOMAIN.'bmis');
	}
	public function get_product()
	{
		echo json_encode($this -> CoreModel -> get_product($_POST['productId']));
	}

	public function get_verticle()
	{
		echo json_encode($this -> CoreModel -> get_verticle($_POST['verticleId']));
	}

	public function get_sourcing_category()
	{
		echo json_encode($this -> CoreModel -> get_sourcing_category($_POST['catId']));
	}

	public function get_source_name()
	{
		echo json_encode($this -> CoreModel -> get_source_name($_POST['sourceCode']));
	}

	public function get_exec_name()
	{
		echo json_encode($this -> CoreModel -> get_exec_name($_POST['execCode']));
	}

	public function bmis_view($id)
	{
		$data['listing'] = $this->CoreModel->get_full_description('bmis','',$where=array('id'=>$id));
		$this -> load -> view('bmis_view', $data);
	}

	
}