<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		checkSession();
	}
	public function index(){
		$first_date = date('Y-m-d',strtotime('first day of this month'));
$last_date = date('Y-m-d',strtotime('last day of this month'));

	if($this->session->userdata('auth')['role_id']==1){
		$where_total_today=array('entry_date'=>date('Y-m-d'));
		$where_total_month1=array('entry_date>='=>$first_date);
		$where_total_month2=array('entry_date<='=>$last_date);
		$sql="SELECT count(`id`) as `y`, `entry_date` as `label` FROM `bmis` WHERE `entry_date`
	    BETWEEN '$first_date' and '$last_date'  group by `entry_date` ";
		$query = $this->db->query($sql);
		//echo $this->db->last_query();die;
		$result = $query->result();
		//var_dump(json_encode($result));die;
		if($result)
		$data['chart']=json_encode($result,JSON_NUMERIC_CHECK);
		else
		$data['chart']=json_encode(array());

	}
	else
	{
		$user_id=$this->session->userdata('auth')['id'];
		$where_total_today=array('user_id'=>$this->session->userdata('auth')['id'],'entry_date'=>date('Y-m-d'));
		$where_total_month1=array('user_id'=>$this->session->userdata('auth')['id'],'entry_date>='=>$first_date);
		$where_total_month2=array('entry_date<='=>$last_date);
		$query= $this->db->query("SELECT count(`id`) as `y`, `entry_date` as `label` FROM `bmis` WHERE `entry_date`
	    BETWEEN '$first_date' and '$last_date' and `user_id`='$user_id'  group by `entry_date` ");
		$result = $query->result();
		if($result)
		$data['chart']=json_encode($result,JSON_NUMERIC_CHECK);
		else
		$data['chart']=json_encode(array());
    }
    $data['today_total']=$this->CoreModel->get_total_count_record('bmis',$where_total_today);	
    $data['month_total']=$this->CoreModel->get_total_count_record('bmis',$where_total_month1,$where_total_month2);	
	
	
	
	
	$this->load->view('dashboard',$data);
	}

	

}
