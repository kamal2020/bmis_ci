// Rough Work Collapse

jQuery(".rough-work").click(function(){
	jQuery(this).siblings('.rough-content').slideToggle("slow");
});

// Study Content Collapse

jQuery(".study h3").click(function(){
	jQuery(this).siblings(".study-content").slideToggle("slow");
});


// Custom file upload
'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'span' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));

//Flat red color scheme for iCheck
// if($('input[type="checkbox"].flat-red, input[type="radio"].flat-red').length > 0){
// 	jQuery('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
// 	  checkboxClass: 'icheckbox_flat-green',
// 	  radioClass: 'iradio_flat-green'
// 	});
// }

   