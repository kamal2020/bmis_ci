<?php
/**
 * Created by PhpStorm.
 * User: Kamal Swami
 * Company : Coretechies
 */
//===========================custom working start===============================================//
function local_time_utc()
{
    $ci = &get_instance();
    date_default_timezone_set($ci->session->userdata('timezone'));
    $date=date('Y-m-d H:i:s');
    $the_date = strtotime($date);
    date_default_timezone_get();
    date("Y-d-mTG:i:sz",$the_date);
    date_default_timezone_set("UTC");
    $UTC_date=date("Y-m-d H:i:s", $the_date);
    return $UTC_date;
}
function utc_to_local($date)
{
    $ci = &get_instance();

    //date_default_timezone_set($ci->session->userdata('timezone'));
    $dt = new DateTime($date, new DateTimeZone('UTC'));
    //return $dt->format('d-m-Y h:i:s A');
    // change the timezone of the object without changing it's time
    $dt->setTimezone(new DateTimeZone($ci->session->userdata('timezone')));
    // format the datetime
    return $dt->format('d-m-Y h:i:s A');
}
function convert_time_zone($date_time, $from_tz, $to_tz)
{
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    $time_object->setTimezone(new DateTimeZone($to_tz));
    return $time_object->format('Y-m-d H:i:s');
}
function convert_time_zone_date($date_time, $from_tz, $to_tz)
{
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    $time_object->setTimezone(new DateTimeZone($to_tz));
    return $time_object->format('Y-m-d');
}
function convert_time_zone_time($date_time, $from_tz, $to_tz)
{
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    $time_object->setTimezone(new DateTimeZone($to_tz));
    return $time_object->format('H:i:s');
}
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function generateRandomStringSmall($length = 5)
{
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

//////////generate identifier//////////////
function get_identifier()
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description('matrimony_profile', 'identifier', $where = array(''), 'matrimony_id desc');
    if ($table_data) {
        $last_identifier = $table_data->identifier;
        // $last_identifier=explode('-',$last_identifier);
        $unique = $last_identifier + 1;

        return $unique;
    } else {
        return '1001';
    }
}

function encript($string)
{
    $key = md5('matripostindia');
    $salt = md5('matripostindia');
    $string = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_ECB)));
    return $string;
}

function decript($string)
{//header('Content-Type: text/html; charset=utf-8');
    $key = md5('matripostindia');
    $salt = md5('matripostindia');
    $string = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($string), MCRYPT_MODE_ECB));
    return $string;
}

function salt($string)
{

    $salt = 'CtE&*(;N)!@#C78R,.Y12P90T(I45O,.N';
    $string = $string . $salt;
    $string = sha1($string);
    return $string;
}

function str_replace_for_url($title)
{
    $url_content = $title;
    $url_content = str_replace('  ', ' ', $url_content);
    $url_content = str_replace('&', '', $url_content);
    $url_content = str_replace(',', '', $url_content);
    $url_content = str_replace('.', '', $url_content);
    $url_content = str_replace('(', '', $url_content);
    $url_content = str_replace(')', '', $url_content);
    $url_content = str_replace('*', '', $url_content);
    $url_content = str_replace('+', '', $url_content);
    $url_content = str_replace('=', '', $url_content);
    $url_content = str_replace('%', '', $url_content);
    $url_content = str_replace('#', '', $url_content);
    $url_content = str_replace('/', '', $url_content);
    $url_content = str_replace('\\', '', $url_content);
    $url_content = str_replace(':', '', $url_content);
    $url_content = str_replace('-', '', $url_content);
    $url_content = str_replace("'", '', $url_content);
    $url_content = str_replace("|", '', $url_content);
    $url_content = str_replace("!", '', $url_content);
    $url_content = str_replace("?", '', $url_content);
    $url_content = str_replace(";", '', $url_content);
    $url_content = str_replace("$", '', $url_content);
    $url_content = str_replace("^", '', $url_content);
    $url_content = str_replace('@', '', $url_content);
    $url_content = str_replace('"', '', $url_content);
    $url_content = str_replace(' ', '-', $url_content);
    return strtolower($url_content);
}

function get_member_block_status($matromony_id)
{
    $ci = &get_instance();
    $ci->load->model('members/Listing_model');

    $table_data = $ci->Listing_model->check_member_already_block($matromony_id);
    if ($table_data) {
        return 'Un-Block';
    } else {
        return 'Block';
    }

}

function get_member_shortlisted_status($matromony_id)
{
    $ci = &get_instance();
    $ci->load->model('members/Listing_model');

    $table_data = $ci->Listing_model->check_member_already_shortlisted($matromony_id);
    if ($table_data) {
        if ($table_data->shortlisted_status == 'Y') {
            return 'Un-Shortlist';
        } else {
            return 'Shortlist';
        }

    } else {
        return 'Shortlist';
    }

}

function get_member_express_interest_status($matromony_id)
{
    $ci = &get_instance();
    $ci->load->model('members/Listing_model');

    $table_data = $ci->Listing_model->check_express_interest_already($matromony_id);
    if ($table_data) {
        if($table_data->response=='Pending')
        return 'Un-Express Interest';
            else
        return $table_data->response;


    } else {
        return 'Express Interest';
    }

}
function get_member_express_interest_status_recived($matromony_id)
{
    $ci = &get_instance();
    $ci->load->model('members/Listing_model');

    $table_data = $ci->Listing_model->check_express_interest_already_recived($matromony_id);
    if ($table_data) {

        return $table_data->response;


    } else {
        return 'Express Interest';
    }

}

/**
 *  this function use for get select field for table
 * @param $table_name ( name of table_name)
 * @param $field_name (name of field)
 * @param $where ( where condition)
 * @return string (return name of field)
 */
function get_field_form_table($table_name, $field_name, $where, $order = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description($table_name, $field_name, $where, $order);
    if ($table_data) {
        return $table_data->$field_name;
    } else {
        return 'not completed';
    }

}
/**
 *  this function use for get select field for table
 * @param $table_name ( name of table_name)
 * @param $field_name (name of field)
 * @param $where ( where condition)
 * @return string (return name of field)
 */
function get_field_form_table_for_pagetitle($table_name, $field_name, $where, $order = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description($table_name, $field_name, $where, $order);
    if ($table_data) {
        return $table_data->$field_name;
    } else {
        return 'Matripost';
    }

}
/**
 *  this function use for get select field for table
 * @param $table_name ( name of table_name)
 * @param $field_name (name of field)
 * @param $where ( where condition)
 * @return string (return name of field)
 */
function get_field_form_table_for_ads($table_name, $field_name, $where, $order = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description($table_name, $field_name, $where, $order);
    if ($table_data) {
        return $table_data->$field_name*1000;
    } else {
        return '300';
    }

}
/**
 * this function use for all form Region dropdown
 * @param null $select_country (already selected region_id )
 * @return string this function return region dropdown
 */
function get_region_dropdown($select_region = NULL)
{
    $ci = &get_instance();
    $table = 'master_region';
    $fields = 'region_code,region';
    $where = array('status' => 'E', 'region !='=>'');
    $group_by = 'region';
    $result_data = $ci->General_model->get_detail_group_by($table, $fields, $where, $group_by);
    $dropdown = '<select name="region_id" id="region_id" class="form-control region_id">';
    $dropdown .= '<option value="">Please Select region</option>';
    foreach ($result_data as $row) {
        if ($row->region_code == $select_region) {
            $dropdown .= '<option value="' . $row->region_code . '" selected>' . $row->region . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->region_code . '">' . $row->region . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 *  this function use for get select field for table
 * @param $table_name ( name of table_name)
 * @param $field_name (name of field)
 * @param $where ( where condition)
 * @return string (return name of field)
 */
function get_country_state_form_city_id($city_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $table_data = $ci->General_model->get_country_state_form_city_id($city_id);
    if ($table_data) {
        return $table_data;
    } else {
        return 'not completed';
    }

}

function get_user_profile_image($matrimony_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $where_condition = array('matrimony_id' => $matrimony_id);
    $table_data = $ci->General_model->get_full_description('matrimony_profile_image', 'profile_image', $where_condition, 'matrimony_profile_image_id asc');
    if ($table_data) {
        return base_url().'upload_dir/member_images/' . $table_data->profile_image;
    } else {
        return base_url() . 'assets/front/images/users/default.png';
    }
}

function get_vendor_profile_image($vendor_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $where_condition = array('vendor_id' => $vendor_id);
    $table_data = $ci->General_model->get_full_description('vendor', 'vendor_profiile_image', $where_condition);
    if ($table_data->vendor_profiile_image) {
        return base_url() . 'upload_dir/vendor_image/' . $table_data->vendor_profiile_image;
    } else {
        return base_url() . 'assets/front/images/users/default.png';
    }
}

function get_matrimony_feature_profile($matrimony_id = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $where_condition = array('matrimony_id !=' => $matrimony_id);
    $table_data = $ci->General_model->get_matrimony_feature_profile_listing('matrimony_feature_profile', 'profile_image', $where_condition, 'rand()');
    if ($table_data) {
        return $table_data;
    } else {
        return '';
    }
}

function get_matrimony_recent_view()
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_matrimony_recent_view_profile_listing();
    if ($table_data) {
        return $table_data;
    } else {
        return '';
    }
}

/**
 * this function use for all form country dropdown
 * @param null $select_country (already selected country id )
 * @return string this function return country dropdown
 */
function get_country_dropdown($select_country = NULL)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="country_id" id="country_id" class="form-control country_id">';
    $dropdown .= '<option value="">Please Select Country</option>';
    foreach ($result_data as $row) {
        if ($row->country_id == $select_country) {
            $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form country dropdown
 * @param null $select_country (already selected country id )
 * @return string this function return country dropdown
 */
function get_currency_dropdown()
{
    $ci = &get_instance();
    $table = 'master_currency';
    $fields = 'country,code,symbol';
   $where = array('code !=' => '', 'symbol !=' => '' ,'status'=>'E');

//$where=array('2','53');
    $order_by = 'country asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
//    $dropdown = '';
//    $dropdown .= '<option value="">Currency</option>';
//    foreach ($result_data as $row) {
//        if ($row->country_id == $select_country) {
//            $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
//        } else {
//            $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
//        }
//
//    }
//
    return $result_data;
}
function get_currency_dropdown_profession($currency=null)
{
    $ci = &get_instance();
    $table = 'master_currency';
    $fields = 'country,code,symbol';
    $where = array('code !=' => '', 'symbol !=' => '','status'=>'E');
    $order_by = 'country asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="profession_currency" id="profession_currency" class="form-control">';
    $dropdown .= '<option value="">Currency</option>';
    foreach ($result_data as $row) {
        if ($row->code == $currency) {
            $dropdown .= '<option value="' . $row->code . '" selected>' . $row->code . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->code . '">' . $row->code . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function create state dropdown
 * @param null $select_country (pass last select country id)
 * @param null $select_state (already select state id)
 * @return string (this function return state dropdown)
 */
function get_state_dropdown($select_country = NULL, $select_state = NULL)
{
    $ci = &get_instance();
    $table = 'master_state';
    $fields = 'state_id,state_name';
    $where = array('country_id' => $select_country, 'status' => 'E');
    $order_by = 'state_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="state_id" id="state_id" class="form-control state_id">';
    $dropdown .= '<option value="">Please Select State</option>';
    foreach ($result_data as $row) {
        if ($row->state_id == $select_state) {
            $dropdown .= '<option value="' . $row->state_id . '" selected>' . $row->state_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->state_id . '">' . $row->state_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function create city dropdown
 * @param null $select_state (pass last select state id)
 * @param null $select_city (already select city id)
 * @return string (this function return city dropdown)
 */
function get_city_dropdown($select_state = NULL, $select_city = NULL)
{
    $ci = &get_instance();
    $table = 'master_city';
    $fields = 'city_id,city_name';
    $where = array('state_id' => $select_state, 'status' => 'E');
    $order_by = 'city_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="city_id" id="city_id" class="form-control city_id">';
    $dropdown .= '<option value="">Please Select City</option>';
    foreach ($result_data as $row) {
        if ($row->city_id == $select_city) {
            $dropdown .= '<option value="' . $row->city_id . '" selected>' . $row->city_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->city_id . '">' . $row->city_name . '</option>';
        }
    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_zip_dropdown($select_city = NULL, $selected_zip = null)
{
    $ci = &get_instance();
    $table = 'master_zip';
    $fields = 'zip_id,zip_code';
    $where = array('city_id' => $select_city);
    $order_by = 'zip_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="zip" id="zip" class="form-control zip">';
    $dropdown .= '<option value="">Please Select Zip-code</option>';
    foreach ($result_data as $row) {
        if ($row->zip_id == $selected_zip) {
            $dropdown .= '<option value="' . $row->zip_id . '" selected>' . $row->zip_code . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->zip_id . '">' . $row->zip_code . '</option>';
        }
    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form country dropdown
 * @param null $select_country (already selected country id )
 * @return string this function return country dropdown
 */
function get_second_country_dropdown($select_country = NULL)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="second_country_id" id="second_country_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Country</option>';
    foreach ($result_data as $row) {
        if ($row->country_id == $select_country) {
            $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function create state dropdown
 * @param null $select_country (pass last select country id)
 * @param null $select_state (already select state id)
 * @return string (this function return state dropdown)
 */
function get_second_state_dropdown($select_country = NULL, $select_state = NULL)
{
    $ci = &get_instance();
    $table = 'master_state';
    $fields = 'state_id,state_name';
    $where = array('country_id' => $select_country, 'status' => 'E');
    $order_by = 'state_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="second_state_id" id="second_state_id" class="form-control">';
    $dropdown .= '<option value="">Please Select State</option>';
    foreach ($result_data as $row) {
        if ($row->state_id == $select_state) {
            $dropdown .= '<option value="' . $row->state_id . '" selected>' . $row->state_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->state_id . '">' . $row->state_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function create city dropdown
 * @param null $select_state (pass last select state id)
 * @param null $select_city (already select city id)
 * @return string (this function return city dropdown)
 */
function get_second_city_dropdown($select_state = NULL, $select_city = NULL)
{
    $ci = &get_instance();
    $table = 'master_city';
    $fields = 'city_id,city_name';
    $where = array('state_id' => $select_state, 'status' => 'E');
    $order_by = 'city_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="second_city_id" id="second_city_id" class="form-control">';
    $dropdown .= '<option value="">Please Select City</option>';
    foreach ($result_data as $row) {
        if ($row->city_id == $select_city) {
            $dropdown .= '<option value="' . $row->city_id . '" selected>' . $row->city_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->city_id . '">' . $row->city_name . '</option>';
        }
    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_second_zip_dropdown($select_city = NULL, $selected_zip = null)
{
    $ci = &get_instance();
    $table = 'master_zip';
    $fields = 'zip_id,zip_code';
    $where = array('city_id' => $select_city);
    $order_by = 'zip_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="second_zip" id="second_zip" class="form-control">';
    $dropdown .= '<option value="">Please Select Zip-code</option>';
    foreach ($result_data as $row) {
        if ($row->zip_id == $selected_zip) {
            $dropdown .= '<option value="' . $row->zip_id . '" selected>' . $row->zip_code . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->zip_id . '">' . $row->zip_code . '</option>';
        }
    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form mothertongue dropdown
 * @param null $select_master_mothertongue (already selected mothertongue id )
 * @return string this function return mothertongue dropdown
 */
function get_master_mothertongue_dropdown($select_master_mothertongue = NULL,$ship_id=null)
{
    $ci = &get_instance();
    $table = 'master_mothertongue';
    $fields = 'mothertongue_id,mothertongue_name';
    if($ship_id)
    $where = array('status' => 'E','mothertongue_id!='=>1);
    else
    $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="mothertongue[]" id="mothertongue" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    $dropdown .= '<option value="" disabled>Select Spoken Language</option>';
    if ($select_master_mothertongue) {
        foreach ($result_data as $row) {
            if (in_array($row->mothertongue_id, json_decode($select_master_mothertongue))) {

                $dropdown .= '<option value="' . $row->mothertongue_id . '" selected>' . $row->mothertongue_name . '</option>';
            } else {

                $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';
        }
    }


    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form religion dropdown
 * @param null $select_religion (already selected religion id )
 * @return string this function return religion dropdown
 */
function get_master_religion_dropdown($select_religion = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_religion';
    $fields = 'religion_id,religion_name';
    if($skip_id)
    $where = array('status' => 'E','religion_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="religion_id" id="religion_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Religion</option>';
    foreach ($result_data as $row) {
        if ($row->religion_id == $select_religion) {
            $dropdown .= '<option value="' . $row->religion_id . '" selected>' . $row->religion_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->religion_id . '">' . $row->religion_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form caste dropdown
 * @param null $religion_id ,$select_master_caste (already selected religion id,caste id )
 * @return string this function return caste dropdown
 */
function get_master_caste_dropdown($religion_id = null, $select_master_caste = NULL)
{
    $ci = &get_instance();
    $table = 'master_caste';
    $fields = 'caste_id,caste_name';
    $where = array('religion_id' => $religion_id, 'status' => 'E');
    $order_by = 'caste_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="caste_id" id="caste_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Caste</option>';
    foreach ($result_data as $row) {
        if ($row->caste_id == $select_master_caste) {
            $dropdown .= '<option value="' . $row->caste_id . '" selected>' . $row->caste_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->caste_id . '">' . $row->caste_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form subcaste dropdown
 * @param null $religion_id ,$select_master_subcaste (already selected body_type id )
 * @return string this function return subcaste dropdown
 */
function get_master_subcaste_dropdown($religion_id = null, $select_master_subcaste = NULL)
{
    $ci = &get_instance();
    $table = 'master_subcaste';
    $fields = 'subcaste_id,subcaste_name';
    $where = array('religion_id' => $religion_id, 'status' => 'E');
    $order_by = 'subcaste_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="subcaste_id" id="subcaste_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Sub Caste</option>';
    foreach ($result_data as $row) {
        if ($row->subcaste_id == $select_master_subcaste) {
            $dropdown .= '<option value="' . $row->subcaste_id . '" selected>' . $row->subcaste_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->subcaste_id . '">' . $row->subcaste_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form gotra dropdown
 * @param null $select_master_gotra (already selected gotra id )
 * @return string this function return gotra dropdown
 */
function get_master_gotra_dropdown($select_master_gotra = NULL)
{
    $ci = &get_instance();
    $table = 'master_gotra';
    $fields = 'gotra_id,gotra_name';
    $where = array('status' => 'E');
    $order_by = 'gotra_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="gotra_id1" id="gotra_id1" class="form-control">';
    $dropdown .= '<option value="">Please Select Gotra</option>';
    foreach ($result_data as $row) {
        if ($row->gotra_id == $select_master_gotra) {
            $dropdown .= '<option value="' . $row->gotra_id . '" selected>' . $row->gotra_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->gotra_id . '">' . $row->gotra_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form gotra2 dropdown
 * @param null $select_master_gotra (already selected gotra2 id )
 * @return string this function return gotra2 dropdown
 */
function get_master_gotra2_dropdown($select_master_gotra = NULL)
{
    $ci = &get_instance();
    $table = 'master_gotra';
    $fields = 'gotra_id,gotra_name';
    $where = array('status' => 'E');
    $order_by = 'gotra_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="gotra_id2" id="gotra_id2" class="form-control">';
    $dropdown .= '<option value="">Please Select Gotra</option>';
    foreach ($result_data as $row) {
        if ($row->gotra_id == $select_master_gotra) {
            $dropdown .= '<option value="' . $row->gotra_id . '" selected>' . $row->gotra_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->gotra_id . '">' . $row->gotra_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form skintone dropdown
 * @param null $select_master_skintone (already selected skintone id )
 * @return string this function return skintone dropdown
 */
function get_master_skintone_dropdown($select_master_skintone = NULL)
{
    $ci = &get_instance();
    $table = 'master_skintone';
    $fields = 'skintone_id,skintone';
    $where = array('status' => 'E');
    $order_by = 'skintone_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="skintone_id" id="skintone_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Skin tone</option>';
    foreach ($result_data as $row) {
        if ($row->skintone_id == $select_master_skintone) {
            $dropdown .= '<option value="' . $row->skintone_id . '" selected>' . $row->skintone . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->skintone_id . '">' . $row->skintone . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form body_type dropdown
 * @param null $select_body_type (already selected body_type id )
 * @return string this function return master_body_type dropdown
 */
function get_master_body_type_dropdown($select_body_type = NULL)
{
    $ci = &get_instance();
    $table = 'master_body_type';
    $fields = 'body_type_id,body_type';
    $where = array('status' => 'E');
    $order_by = 'body_type_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="body_type_id" id="body_type_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Body Type</option>';
    foreach ($result_data as $row) {
        if ($row->body_type_id == $select_body_type) {
            $dropdown .= '<option value="' . $row->body_type_id . '" selected>' . $row->body_type . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->body_type_id . '">' . $row->body_type . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form height dropdown
 * @param null $select_master_height (already selected height id )
 * @return string this function return height dropdown
 */
function get_master_height_dropdown($select_master_height = NULL)
{
    $ci = &get_instance();
    $table = 'master_height';
    $fields = 'height_id,height';
    $where = array();
    $order_by = 'height_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="height_id" id="height_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Height</option>';
    foreach ($result_data as $row) {
        if ($row->height_id == $select_master_height) {
            $dropdown .= '<option value="' . $row->height_id . '" selected>' . $row->height . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->height_id . '">' . $row->height . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form blood_group dropdown
 * @param null $select_blood_group (already selected blood_group id )
 * @return string this function return blood_group dropdown
 */
function get_master_blood_group_dropdown($select_blood_group = NULL)
{
    $ci = &get_instance();
    $table = 'master_blood_group';
    $fields = 'blood_group_id,blood_group';
    $where = array('status' => 'E');
    $order_by = 'blood_group_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="blood_group_id" id="blood_group_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Blood Group</option>';
    foreach ($result_data as $row) {
        if ($row->blood_group_id == $select_blood_group) {
            $dropdown .= '<option value="' . $row->blood_group_id . '" selected>' . $row->blood_group . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->blood_group_id . '">' . $row->blood_group . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form personality dropdown
 * @param null $select_master_personality (already selected personality id )
 * @return string this function return personality dropdown
 */
function get_master_personality_dropdown($select_master_personality = NULL,$skip_id=null)
{
    //var_dump(json_decode($select_master_personality));die;
    $ci = &get_instance();
    $table = 'master_personality';
    $fields = 'personality_id,personality';
    if($skip_id)
    $where = array('status' => 'E','personality_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'personality_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="personality_id[]" id="personality_id" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="0" >None</option>';
    $i=0;
    if ($select_master_personality) {
        foreach ($result_data as $row) {
            if (in_array($row->personality_id, json_decode($select_master_personality))) {

                $dropdown .= '<option value="' . $row->personality_id . '" selected>' . $row->personality . '</option>';
            } else {

                $dropdown .= '<option value="' . $row->personality_id . '">' . $row->personality . '</option>';
            }
            $i++;
        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<option value="' . $row->personality_id . '">' . $row->personality . '</option>';
        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form hobbies dropdown
 * @param null $select_master_hobbies (already selected hobbies id )
 * @return string this function return hobbies dropdown
 */
function get_master_hobbies_dropdown($select_master_hobbies = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_hobbies';
    $fields = 'hobbies_id,hobbies';
    if($skip_id)
    $where = array('status' => 'E','hobbies_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'hobbies_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="hobbies_id[]" id="hobbies_id" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="0" >None</option>';
    if ($select_master_hobbies) {
        foreach ($result_data as $row) {
            if (in_array($row->hobbies_id, json_decode($select_master_hobbies))) {
                $dropdown .= '<option value="' . $row->hobbies_id . '" selected>' . $row->hobbies . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->hobbies_id . '">' . $row->hobbies . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<option value="' . $row->hobbies_id . '">' . $row->hobbies . '</option>';

        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form medical_condition dropdown
 * @param null $select_master_medical_condition (already selected medical_condition id )
 * @return string this function return medical_condition dropdown
 */
function get_master_medical_condition_dropdown($select_master_medical_condition = NULL)
{
    $ci = &get_instance();
    $table = 'master_medical_condition';
    $fields = 'medical_condition_id,medical_condition';
    $where = array('status' => 'E');
    $order_by = 'medical_condition_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="medical_condition_id[]" id="medical_condition_id" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="">Any</option>';

    if ($select_master_medical_condition) {
        foreach ($result_data as $row) {
            if (in_array($row->medical_condition_id, json_decode($select_master_medical_condition))) {
                $dropdown .= '<option value="' . $row->medical_condition_id . '" selected>' . $row->medical_condition . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->medical_condition_id . '">' . $row->medical_condition . '</option>';
            }
        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<option value="' . $row->medical_condition_id . '">' . $row->medical_condition . '</option>';
        }
    }


    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form timezone dropdown
 * @param null $select_timezone (already timezone id )
 * @return string this function return timezone dropdown
 */
function get_master_timezone_dropdown($select_timezone = NULL)
{
    $ci = &get_instance();
    $table = 'master_timezone';
    $fields = 'timezone_id,timezone,offset';
    $where = array('status' => 'E');
    $order_by = 'timezone_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="timezone" id="timezone" class="form-control">';
    $dropdown .= '<option value="">Please Select Timezone</option>';
    foreach ($result_data as $row) {
        if ($row->timezone_id == $select_timezone) {
            $dropdown .= '<option value="' . $row->timezone_id . '" selected>' . $row->timezone . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->timezone_id . '">' . $row->timezone . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form education dropdown
 * @param null $select_education (already selected education id )
 * @return string this function return education dropdown
 */
function get_education_dropdown($select_education = NULL ,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_education';
    $fields = 'education_id,education';
    if($skip_id)
        $where = array('status' => 'E','education_id!='=>$skip_id);
    else
        $where = array('status' => 'E');
    $order_by = 'education_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="education_id" id="education_id" class="form-control">';
    $dropdown .= '<option value="" >Please Select Educational Level</option>';
    foreach ($result_data as $row) {
        if ($row->education_id == $select_education) {
            $dropdown .= '<option value="' . $row->education_id . '" selected>' . $row->education . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->education_id . '">' . $row->education . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
function get_education_field_dropdown($select_education = NULL)
{
    $ci = &get_instance();
    $table = 'master_education_field';
    $fields = 'education_field_id,education_field';
    $where = array('status' => 'E');
    $order_by = 'education_field asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="education_field" id="education_field" class="form-control">';
    $dropdown .= '<option value="" >Please Select Field of Study</option>';
    foreach ($result_data as $row) {
        if ($row->education_field_id == $select_education) {
            $dropdown .= '<option value="' . $row->education_field_id . '" selected>' . $row->education_field . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->education_field_id . '">' . $row->education_field . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function use for get profession dropdown
 * @param null $select_profession (already selected profession id )
 * @return string this function return profession dropdown
 */
function get_profession_dropdown($select_profession = NULL ,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_profession';
    $fields = 'profession_id,profession';
    if($skip_id)
        $where = array('status' => 'E','profession_id!='=>$skip_id);
    else
        $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="profession_id" class="form-control">';
    $dropdown .= '<option value="" >Please Select Profession</option>';
    foreach ($result_data as $row) {
        if ($row->profession_id == $select_profession) {
            $dropdown .= '<option value="' . $row->profession_id . '" selected>' . $row->profession . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->profession_id . '">' . $row->profession . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/////////////////////////////////for desired-partner page////////////////////////////////////////////////////
/**
 * this function use for all form height dropdown
 * @param null $select_master_height (already selected height id )
 * @return string this function return height dropdown
 */
function get_master_desire_from_height_dropdown($select_master_height = NULL)
{
    $ci = &get_instance();
    $table = 'master_height';
    $fields = 'height_id,height';
    $where = array();
    $order_by = 'height_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_fromheight_id" id="desire_fromheight_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Height</option>';
    foreach ($result_data as $row) {
        if ($row->height_id == $select_master_height) {
            $dropdown .= '<option value="' . $row->height_id . '" selected>' . $row->height . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->height_id . '">' . $row->height . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_master_desire_to_height_dropdown($select_master_height = NULL)
{
    $ci = &get_instance();
    $table = 'master_height';
    $fields = 'height_id,height';
    $where = array();
    $order_by = 'height_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_toheight_id" id="desire_toheight_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Height</option>';
    foreach ($result_data as $row) {
        if ($row->height_id == $select_master_height) {
            $dropdown .= '<option value="' . $row->height_id . '" selected>' . $row->height . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->height_id . '">' . $row->height . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form mothertongue dropdown
 * @param null $select_master_mothertongue (already selected mothertongue id )
 * @return string this function return mothertongue dropdown
 */
function get_master_desire_mothertongue_dropdown($select_master_mothertongue = NULL,$add=null)
{
    $ci = &get_instance();
    $table = 'master_mothertongue';
    $fields = 'mothertongue_id,mothertongue_name';
    if($add)
    {
        $where = array('status' => 'E');
    }
    else
    {
        $where = array('status' => 'E' ,'mothertongue_id>'=>2);
    }

    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_mothertongue[]" id="desire_mothertongue" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="" disabled>Please Select Religion</option>';
    if ($select_master_mothertongue) {
        foreach ($result_data as $row) {
            if (in_array($row->mothertongue_id, json_decode($select_master_mothertongue))) {
                $dropdown .= '<option value="' . $row->mothertongue_id . '" selected>' . $row->mothertongue_name . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';

        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

function get_master_desire_mothertongue_dropdown1($select_master_mothertongue = NULL,$add=null)
{
    //var_dump($add);die;
    $ci = &get_instance();
    $table = 'master_mothertongue';
    $fields = 'mothertongue_id,mothertongue_name';
    if($add)
    {
        $where = array('status' => 'E' ,'mothertongue_id>'=>'2');

    }
    else
    {
        $where = array('status' => 'E');
    }

    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_mothertongue[]" id="desire_mothertongue" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="" disabled>Please Select Religion</option>';
    if ($select_master_mothertongue) {
        foreach ($result_data as $row) {
            if (in_array($row->mothertongue_id, json_decode($select_master_mothertongue))) {
                $dropdown .= '<option value="' . $row->mothertongue_id . '" selected>' . $row->mothertongue_name . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';

        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function use for all form religion dropdown
 * @param null $select_religion (already selected religion id )
 * @return string this function return religion dropdown
 */
function get_master_desire_religion_dropdown($select_religion = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_religion';
    $fields = 'religion_id,religion_name';
    if($skip_id)
    $where = array('status' => 'E','religion_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_religion[]" id="desire_religion" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
   // $dropdown .= '<option value="" disabled>Please Select Religion</option>';
    if ($select_religion) {
        foreach ($result_data as $row) {
            if (in_array($row->religion_id, json_decode($select_religion))) {
                $dropdown .= '<option value="' . $row->religion_id . '" selected>' . $row->religion_name . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->religion_id . '">' . $row->religion_name . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<option value="' . $row->religion_id . '">' . $row->religion_name . '</option>';

        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form caste dropdown
 * @param null $religion_id ,$select_master_caste (already selected religion id,caste id )
 * @return string this function return caste dropdown
 */
function get_master_desire_caste_dropdown($religion_id = null, $select_master_caste = NULL)
{
    $ci = &get_instance();
    $table = 'master_caste';
    $fields = 'caste_id,caste_name';
    $where = array('religion_id' => $religion_id, 'status' => 'E');
    $order_by = 'caste_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_caste[]" id="desire_caste" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    $dropdown .= '<option value="">Please Select Caste</option>';
    foreach ($result_data as $row) {
        if (in_array($row->caste_id, json_decode($select_master_caste))) {

            $dropdown .= '<option value="' . $row->caste_id . '" selected>' . $row->caste_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->caste_id . '">' . $row->caste_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form subcaste dropdown
 * @param null $religion_id ,$select_master_subcaste (already selected body_type id )
 * @return string this function return subcaste dropdown
 */
function get_master_desire_subcaste_dropdown($religion_id = null, $select_master_subcaste = NULL)
{
    $ci = &get_instance();
    $table = 'master_subcaste';
    $fields = 'subcaste_id,subcaste_name';
    $where = array('religion_id' => $religion_id, 'status' => 'E');
    $order_by = 'subcaste_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_subcaste[]" id="desire_subcaste" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    $dropdown .= '<option value="">Please Select Sub Caste</option>';
    foreach ($result_data as $row) {
        if (in_array($row->subcaste_id, json_decode($select_master_subcaste))) {

            $dropdown .= '<option value="' . $row->subcaste_id . '" selected>' . $row->subcaste_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->subcaste_id . '">' . $row->subcaste_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for all form personality dropdown
 * @param null $select_master_personality (already selected personality id )
 * @return string this function return personality dropdown
 */
function get_master_desire_personality_dropdown($select_master_personality = NULL,$skip_id=null)
{
    //var_dump(json_decode($select_master_personality));die;
    $ci = &get_instance();
    $table = 'master_personality';
    $fields = 'personality_id,personality';
    if($skip_id)
    $where = array('status' => 'E','personality_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'personality_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_personality[]" id="desire_personality" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="" disabled>Please Select Personality</option>';
    if ($select_master_personality) {
        foreach ($result_data as $row) {
            if (in_array($row->personality_id, json_decode($select_master_personality))) {
                $dropdown .= '<option value="' . $row->personality_id . '" selected>' . $row->personality . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->personality_id . '">' . $row->personality . '</option>';
            }
        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<option value="' . $row->personality_id . '">' . $row->personality . '</option>';
        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

function get_master_desire_personality_dropdown1($select_master_personality = NULL,$skip_id=null)
{
    //var_dump(json_decode($select_master_personality));die;
    $ci = &get_instance();
    $table = 'master_personality';
    $fields = 'personality_id,personality';
    if($skip_id)
        $where = array('status' => 'E','personality_id>'=>2);
    else
        $where = array('status' => 'E');
    $order_by = 'personality_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_personality[]" id="desire_personality" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="" disabled>Please Select Personality</option>';
    if ($select_master_personality) {
        foreach ($result_data as $row) {
            if (in_array($row->personality_id, json_decode($select_master_personality))) {
                $dropdown .= '<option value="' . $row->personality_id . '" selected>' . $row->personality . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->personality_id . '">' . $row->personality . '</option>';
            }
        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<option value="' . $row->personality_id . '">' . $row->personality . '</option>';
        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function use for all form education dropdown
 * @param null $select_education (already selected education id )
 * @return string this function return education dropdown
 */
function get_desire_education_dropdown($select_education = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_education';
    $fields = 'education_id,education';
    if($skip_id)
    $where = array('status' => 'E','education_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'education_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_education[]" id="desire_education"  class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="" disabled>Please Select Educational Level</option>';
    if ($select_education) {
        foreach ($result_data as $row) {
            if (in_array($row->education_id, json_decode($select_education))) {
                $dropdown .= '<option value="' . $row->education_id . '" selected>' . $row->education . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->education_id . '">' . $row->education . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<option value="' . $row->education_id . '">' . $row->education . '</option>';
        }


    }

    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function use for get profession dropdown
 * @param null $select_profession (already selected profession id )
 * @return string this function return profession dropdown
 */
function get_desire_profession_dropdown($select_profession = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_profession';
    $fields = 'profession_id,profession';
    if($skip_id)
        $where = array('status' => 'E','profession_id!='=>$skip_id);
    else
        $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_profession[]" id="desire_profession" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    //$dropdown .= '<option value="" disabled>Please Select Profession</option>';
    if ($select_profession) {
        foreach ($result_data as $row) {
            if (in_array($row->profession_id, json_decode($select_profession))) {
                $dropdown .= '<option value="' . $row->profession_id . '" selected>' . $row->profession . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->profession_id . '">' . $row->profession . '</option>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<option value="' . $row->profession_id . '">' . $row->profession . '</option>';
        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function get_multipel_data($table, $fields, $check, $select_personality)
{

    $ci = &get_instance();
    $result_data = $ci->General_model->get_multipel_data($table, $fields, $check, $select_personality);
    return $result_data;
}

function dateDiff($date)
{
    $mydate = date("Y-m-d H:i:s");
    $theDiff = "";
    //echo $mydate;//2014-06-06 21:35:55
    $datetime1 = date_create($date);
    $datetime2 = date_create($mydate);
    $interval = date_diff($datetime1, $datetime2);
    //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
    $min = $interval->format('%i');
    $sec = $interval->format('%s');
    $hour = $interval->format('%h');
    $mon = $interval->format('%m');
    $day = $interval->format('%d');
    $year = $interval->format('%y');
    if ($interval->format('%i%h%d%m%y') == "00000") {
        //echo $interval->format('%i%h%d%m%y')."<br>";
        return $sec . " Seconds Ago";

    } else if ($interval->format('%h%d%m%y') == "0000") {
        return $min . " Minutes Ago";
    } else if ($interval->format('%d%m%y') == "000") {
        return $hour . " Hours Ago";
    } else if ($interval->format('%m%y') == "00") {
        return $day . " Days Ago";
    } else if ($interval->format('%y') == "0") {
        return $mon . " Months Ago";
    } else {
        return $year . " Years Ago";
    }

}

function truncateString($str, $chars, $to_space, $replacement = "...")
{
    if ($chars > strlen($str)) return $str;

    $str = substr($str, 0, $chars);
    $space_pos = strrpos($str, " ");
    if ($to_space && $space_pos >= 0)
        $str = substr($str, 0, strrpos($str, " "));

    return ($str . $replacement);
}

function get_customer_for_auto_complete()
{
    $ci = &get_instance();
    // get category listing details
    //get user details
    $select_fild = 'matrimony_id,first_name,last_name';
    $where_condition = array(
        'status_id' => '1'
    );

    $customer_details = $ci->General_model->get_user_search_list('matrimony_profile', $select_fild, $where_condition);


    //var_dump($customer_details);
    return json_encode($customer_details);
}
function get_customer_for_auto_complete_message()
{
    $ci = &get_instance();
    // get category listing details
    //get user details
    $select_fild = 'matrimony_id,first_name,last_name';
    $where_condition = array(
        'status_id' => '1'
    );

    $customer_details = $ci->General_model->get_user_search_list_message('matrimony_profile', $select_fild, $where_condition);


    //var_dump($customer_details);
    return json_encode($customer_details);
}
function unique_multidim_array($array, $key)
{
    $temp_array = array();
    $i = 0;
    $key_array = array();
    foreach ($array as $val1) {
        foreach ($val1 as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
    }
    return $temp_array;
}

function get_count($table, $where = null)
{
    $ci = &get_instance();
    $count = $ci->General_model->get_total_count_record($table, $where);
    if ($count) {
        return $count;
    } else {
        return 0;
    }
}

function get_user_name_and_identifier($member_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description('matrimony_profile', 'identifier,first_name,last_name', $where = array('matrimony_id' => $member_id), 'matrimony_id desc');
    if ($table_data) {
        $user = $ci->encrypt->decode(trim($table_data->first_name)) . ' ' . $ci->encrypt->decode(trim($table_data->last_name)) . ' ( #' . $table_data->identifier . ' )';

        return $user;
    } else {
        return '';
    }
}
function get_user_name($member_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description('matrimony_profile', 'first_name,last_name', $where = array('matrimony_id' => $member_id), 'matrimony_id desc');
    if ($table_data) {
        $user = $ci->encrypt->decode(trim($table_data->first_name)) . ' ' . $ci->encrypt->decode(trim($table_data->last_name));

        return $user;
    } else {
        return '';
    }
}
function get_user_email($member_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description('matrimony_profile', 'reg_email', $where = array('matrimony_id' => $member_id), 'matrimony_id desc');
    if ($table_data) {
        $user = $ci->encrypt->decode(trim($table_data->reg_email));

        return $user;
    } else {
        return '';
    }
}
function get_search_religion_dropdown($select_religion = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_religion';
    $fields = 'religion_id,religion_name';
    if($skip_id)
    $where = array('status' => 'E','religion_id!='=>1);
    else
    $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_religion" id="desire_religion" class="form-control">';
    $dropdown .= '<option value="">Religion</option>';
    foreach ($result_data as $row) {
        if ($row->religion_id == $select_religion) {
            $dropdown .= '<option value="' . $row->religion_id . '" selected>' . $row->religion_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->religion_id . '">' . $row->religion_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_search_country_dropdown($select_country = NULL)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_country[]" id="country_id" class="form-control country_id">';
    $dropdown .= '<option value="">Country</option>';
    foreach ($result_data as $row) {
        if ($row->country_id == $select_country) {
            $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_search_profession_dropdown($select_profession = NULL ,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_profession';
    $fields = 'profession_id,profession';
    if($skip_id)
    $where = array('status' => 'E','profession_id>'=>2);
    else
        $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="desire_profession" class="form-control">';
    $dropdown .= '<option value=""  >Profession</option>';
    foreach ($result_data as $row) {
        if ($row->profession_id == $select_profession) {
            $dropdown .= '<option value="' . $row->profession_id . '" selected>' . $row->profession . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->profession_id . '">' . $row->profession . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_search_body_type_dropdown($select_body_type = NULL)
{
    $ci = &get_instance();
    $table = 'master_body_type';
    $fields = 'body_type_id,body_type';
    $where = array('status' => 'E');
    $order_by = 'body_type_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="body_type_id[]" id="body_type_id" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    $dropdown .= '<option value="" disabled>Select Body Type</option>';
    foreach ($result_data as $row) {
        if ($row->body_type_id == $select_body_type) {
            $dropdown .= '<option value="' . $row->body_type_id . '" selected>' . $row->body_type . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->body_type_id . '">' . $row->body_type . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_search_skintone_dropdown($select_master_skintone = NULL)
{
    $ci = &get_instance();
    $table = 'master_skintone';
    $fields = 'skintone_id,skintone';
    $where = array('status' => 'E');
    $order_by = 'skintone_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="skintone_id[]" id="skintone_id" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    $dropdown .= '<option value="" disabled>Select Skin tone</option>';
    foreach ($result_data as $row) {
        if ($row->skintone_id == $select_master_skintone) {
            $dropdown .= '<option value="' . $row->skintone_id . '" selected>' . $row->skintone . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->skintone_id . '">' . $row->skintone . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_user_last_login($user_id, $user_type = null)
{
    $ci = &get_instance();
    $where = array('user_id' => $user_id, 'user_type' => $user_type);
    $result_data = $ci->General_model->get_full_description('matrimony_login_log', 'login_date', $where, 'log_id desc');

    if ($result_data) {
        $date = $result_data->login_date;

        $current_date = date("Y-m-d");
        $a = new DateTime($current_date);
        $b = new DateTime($date);
        $years = $b->diff($a)->y;
        if ($years > 0) {
            return $years . ' Years Ago';
        } else {
            $now = time(); // or your date as well
            $your_date = strtotime($date);
            $datediff = $now - $your_date;
            $days = floor($datediff / (60 * 60 * 24));

            if ($days > 0 && $days < 31) {
                return $days . ' Days Ago';
            } else if ($days <= 0) {
                return 'Today';
            } else {
                $date1 = new DateTime($date);
                $date2 = new DateTime($current_date);
                $interval = date_diff($date1, $date2);
                return $interval->m + ($interval->y * 12) . ' Months Ago';
            }

        }
    } else {
        return 'N/A';
    }


}

//////////////////////Left panel check box functions////////////////////
function get_desire_education_checkbox($select_education = NULL, $search_result = null, $count = null)
{

    $ci = &get_instance();
    $table = 'master_education';
    $fields = 'education_id,education';
    $where = array('status' => 'E','education_id>'=>2);
    $order_by = 'education_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '';
    $dropdown .= '<li><div class="checkbox"><input type="checkbox" name="desire_education[]" id="desire_education" value="" ><label>Doesnt Matter</label><span class="batch">' . $count . '</span></div></li>';
    if ($select_education != 'null') {
        foreach ($result_data as $row) {
            if (in_array($row->education_id, json_decode($select_education))) {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" checked="checked" name="desire_education[]" id="desire_education" value="' . $row->education_id . '" ><label>' . $row->education . '</label><span class="batch">' . function_count_bages($search_result, 'education_id', $row->education_id) . '</span></div></li>';
            } else {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" name="desire_education[]" id="desire_education" value="' . $row->education_id . '" ><label>' . $row->education . '</label><span class="batch">' . function_count_bages($search_result, 'education_id', $row->education_id) . '</span></div></li>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<li><div class="checkbox"><input type="checkbox" name="desire_education[]" id="desire_education" value="' . $row->education_id . '" ><label>' . $row->education . '</label><span class="batch">' . function_count_bages($search_result, 'education_id', $row->education_id) . '</span></div></li>';
        }


    }


    return $dropdown;
}

function get_desire_country_checkbox($select_country = NULL, $search_result = null, $count = null)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '';
    $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_country[]" id="desire_country" value="" ><label>Doesnt Matter</label><span class="batch">' . $count . '</span></div></li>';

    if ($select_country != 'null' && $select_country!='') {
        foreach ($result_data as $row) {
            if (in_array($row->country_id, json_decode($select_country))) {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" checked="checked" name="desire_country[]" id="desire_country" value="' . $row->country_id . '" ><label>' . $row->country_name . '</label><span class="batch">' . function_count_bages($search_result, 'country_id', $row->country_id) . '</span></div></li>';
            } else {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_country[]" id="desire_country" value="' . $row->country_id . '" ><label>' . $row->country_name . '</label><span class="batch">' . function_count_bages($search_result, 'country_id', $row->country_id) . '</span></div></li>';
            }
        }
    } else {

        foreach ($result_data as $row) {
            $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_country[]" id="desire_country" value="' . $row->country_id . '" ><label>' . $row->country_name . '</label><span class="batch">' . function_count_bages($search_result, 'country_id', $row->country_id) . '</span></div></li>';
        }
    }


    return $dropdown;
}

function get_desire_religion_checkbox($select_religion = NULL, $search_result = null, $count = null)
{

    $ci = &get_instance();
    $table = 'master_religion';
    $fields = 'religion_id,religion_name';
    $where = array('status' => 'E','religion_id>'=>1);
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '';
    $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_religion[]" id="desire_religion" value="" ><label>Doesnt Matter</label><span class="batch">' . $count . '</span></div></li>';
    if ($select_religion!='') {
        foreach ($result_data as $row) {
            if (in_array($row->religion_id, json_decode($select_religion))) {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" class="filter_religion" checked="checked"  name="desire_religion[]" id="desire_religion" value="' . $row->religion_id . '" ><label>' . $row->religion_name . '</label><span class="batch">' . function_count_bages($search_result, 'religion_id', $row->religion_id) . '</span></div></li>';
            } else {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" class="filter_religion"  name="desire_religion[]" id="desire_religion" value="' . $row->religion_id . '" ><label>' . $row->religion_name . '</label><span class="batch">' . function_count_bages($search_result, 'religion_id', $row->religion_id) . '</span></div></li>';
            }

        }
    } else {
        foreach ($result_data as $row) {

            $dropdown .= '<li><div class="checkbox"><input type="checkbox" class="filter_religion"  name="desire_religion[]" id="desire_religion" value="' . $row->religion_id . '" ><label>' . $row->religion_name . '</label><span class="batch">' . function_count_bages($search_result, 'religion_id', $row->religion_id) . '</span></div></li>';

        }
    }


    return $dropdown;
}

function get_desire_mothertongue_checkbox($select_master_mothertongue = NULL, $search_result = null, $count = null)
{
    $ci = &get_instance();
    $table = 'master_mothertongue';
    $fields = 'mothertongue_id,mothertongue_name';
    $where = array('status' => 'E' ,'mothertongue_id>'=>2);
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '';
    $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_mothertongue[]" id="desire_mothertongue" value="" ><label>Doesnt Matter</label><span class="batch">' . $count . '</span></div></li>';

    if ($select_master_mothertongue != 'null' && $select_master_mothertongue!='') {
        foreach ($result_data as $row) {
            if (in_array($row->mothertongue_id, json_decode($select_master_mothertongue))) {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" checked="checked"  name="desire_mothertongue[]" id="desire_mothertongue" value="' . $row->mothertongue_id . '" ><label>' . $row->mothertongue_name . '</label><span class="batch"></span></div></li>';

            } else {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_mothertongue[]" id="desire_mothertongue" value="' . $row->mothertongue_id . '" ><label>' . $row->mothertongue_name . '</label><span class="batch"></span></div></li>';

            }

        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_mothertongue[]" id="desire_mothertongue" value="' . $row->mothertongue_id . '" ><label>' . $row->mothertongue_name . '</label><span class="batch">' . function_count_bages($search_result, 'mothertongue_id', $row->mothertongue_id) . '</span></div></li>';

        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

function get_desire_profession_checkbox($select_profession = NULL, $search_result = null, $count = null)
{
    $ci = &get_instance();
    $table = 'master_profession';
    $fields = 'profession_id,profession';
    $where = array('status' => 'E','profession_id>'=>2);
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '';
    $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_profession[]" id="desire_profession" value="" ><label>Doesnt Matter</label><span class="batch">' . $count . '</span></div></li>';
    if ($select_profession != 'null' && $select_profession!='') {
        foreach ($result_data as $row) {
            if (in_array($row->profession_id, json_decode($select_profession))) {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox" checked="checked"  name="desire_profession[]" id="desire_profession" value="' . $row->profession_id . '" ><label>' . $row->profession . '</label><span class="batch"></span></div></li>';
            } else {
                $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_profession[]" id="desire_profession" value="' . $row->profession_id . '" ><label>' . $row->profession . '</label><span class="batch"></span></div></li>';
            }

        }
    } else {
        foreach ($result_data as $row) {
            $dropdown .= '<li><div class="checkbox"><input type="checkbox"  name="desire_profession[]" id="desire_profession" value="' . $row->profession_id . '" ><label>' . $row->profession . '</label><span class="batch">' . function_count_bages($search_result, 'profession_id', $row->profession_id) . '</span></div></li>';
        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

function function_count_bages($result, $field, $val)
{
    $ci = &get_instance();
    $i = 0;
    if ($result) {

        if ($field == 'photo') {
            foreach ($result as $raw) {
                if ($val == 'Y') {
                    if ($raw->matrimony_profile_image_id > 0) {
                        $i++;
                    }
                }
                if ($val == 'N') {
                    if ($raw->matrimony_profile_image_id == '') {
                        $i++;
                    }
                }
            }
        } else if ($field == 'active') {

            foreach ($result as $raw) {

                $id[] = $raw->matrimony_id;
            }
            $result_data = $ci->Search_model->profile_counts($id);
            if ($result_data) {
                if ($val == 'inday') {
                    return $result_data[0]->todayactive;
                } else if ($val == 'inweek') {
                    return $result_data[0]->thisweekactive;
                } else {
                    return $result_data[0]->thismonthactive;
                }
            }
        } else {
            foreach ($result as $raw) {
                //foreach($raw as $raw1) {
                //var_dump($raw);
                // die;
                if ($raw->$field == $val) {
                    $i++;
                }
                // }

            }
        }


        return $i;
    } else {
        return $i;
    }
}

function get_plan_feature($plan_id = NULL, $type = null)
{
    $ci = &get_instance();
    $result_data = $ci->General_model->get_plan_feature_details($plan_id, $type);
    return $result_data;
}

//,$currency_input
//function convert_currency($currency_from=null,$currency_to=null)
//{
//set_time_limit ( 240 );
//$amount = urlencode($currency_input);
//$fromCurrency = urlencode($currency_from);
//$toCurrency = urlencode($currency_to);
//$rawdata = file_get_contents("http://www.google.com/finance/converter?a=$amount&from=$fromCurrency&to=$toCurrency");
//var_dump($rawdata);die;
// $data = explode('bld>', $rawdata);
// $data = explode($toCurrency, $data[1]);
// return round($data[0], 2);
// return ;
//}
function exchange($currency_from, $currency_to)
{
$con=$currency_from.'_'.$currency_to;
    set_time_limit(0);
    $amount = urlencode(1);
    $fromCurrency = urlencode($currency_from);
    $toCurrency = urlencode($currency_to); 
    $rawdata = file_get_contents("https://free.currencyconverterapi.com/api/v5/convert?q=$con&compact=y");
    $data=json_decode($rawdata);
    //var_dump($data->$con->val);die;
    
   
    
    if ($data) {
        return round($data->$con->val, 2);
    } else {
        $this->session->set_userdata("default_currency", 'USD');
        return $amount;
    }

}




function exchange2($currency_from, $currency_to, $currency_input = 1)
{
    error_reporting(0);
    $ci = &get_instance();
    set_time_limit(0);
    $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
    $yql_query = 'select * from yahoo.finance.xchange where pair in ("' . $currency_from . $currency_to . '")';
    $yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
    $yql_query_url .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    $yql_session = file_get_contents($yql_query_url);
    $yql_json = json_decode($yql_session, true);
    $currency_output = (float)$currency_input * $yql_json['query']['results']['rate']['Rate'];
    
//var_dump($currency_output);die;
if ($currency_output) {
        return round($currency_output);
    } else {
        $ci->session->set_userdata("default_currency", 'USD');
        return $currency_input;
    }


}

function exchange1($currency_from, $currency_to)
{
    set_time_limit(0);
    $amount = urlencode(1);
    $fromCurrency = urlencode($currency_from);
    $toCurrency = urlencode($currency_to);
    $rawdata = file_get_contents("http://www.google.com/finance/converter?a=$amount&from=$fromCurrency&to=$toCurrency");
    //var_dump($rawdata);die;
    $data = explode('bld>', $rawdata);
   
    $data = explode($toCurrency, $data[1]);
    if ($data) {
        return round($data[0], 2);
    } else {
        $this->session->set_userdata("default_currency", 'USD');
        return $amount;
    }

}

function get_search_astro_dropdown($selected=null)
{
    $ci = &get_instance();
    $table = 'vendor';
    $fields = 'company_type,company_name,first_name,last_name,vendor_id';
    $where = array('type' => 'A', 'status' => 'E');
    $order_by = 'first_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="select_astro_id" id="select_astro_id" class="selectpicker show-tick form-control" data-live-search="true" >';
    $dropdown .= '<option value="" >Select Astrologer</option>';
    foreach ($result_data as $row) {
        if($row->company_type=='Company')
        {$name=$row->company_name;}else{$name=$row->first_name . ' ' . $row->last_name;}

        if ($row->vendor_id == $selected) {
            $dropdown .= '<option value="' . $row->vendor_id . '" selected>' . $name . '</option>';
        }
        else
        {
            $dropdown .= '<option value="' . $row->vendor_id . '">' .$name. '</option>';
        }


    }
    $dropdown .= '</select>';
    return $dropdown;
}

function ConvertOneTimezoneToAnotherTimezone($time, $currentTimezone, $timezoneRequired)
{
    $system_timezone = date_default_timezone_get();
    $local_timezone = $currentTimezone;
    date_default_timezone_set($local_timezone);
    $local = date("Y-m-d h:i:s A");

    date_default_timezone_set("GMT");
    $gmt = date("Y-m-d h:i:s A");

    $require_timezone = $timezoneRequired;
    date_default_timezone_set($require_timezone);
    $required = date("Y-m-d h:i:s A");

    date_default_timezone_set($system_timezone);

    $diff1 = (strtotime($gmt) - strtotime($local));
    $diff2 = (strtotime($required) - strtotime($gmt));

    $date = new DateTime($time);
    $date->modify("+$diff1 seconds");
    $date->modify("+$diff2 seconds");
    $timestamp = $date->format("Y-m-d H:i:s");
    return $timestamp;
}

function create_time_range($start, $end, $interval, $format = '12', $vander_id = null, $appointment_date = null ,$session_time=null,$timezone=null)
{
    //get appointment date start & end Time
    $start_date_time=$appointment_date.' '.$start;
    $end_date_time=$appointment_date.' '.$end;

    //////conver to timezone
//$ci = &get_instance();
//date_default_timezone_set($ci->session->userdata('timezone'));
//$utc = $start;
//$time = strtotime($utc);
//$start_date_time = date("Y-m-d H:i:s", $time);
 
//$utc = $end;
//$time = strtotime($utc);
//$end_date_time = date("Y-m-d H:i:s", $time);



    //$start_date_time=ConvertOneTimezoneToAnotherTimezone($start_date_time,$timezone,date_default_timezone_get());
    //$end_date_time=ConvertOneTimezoneToAnotherTimezone($end_date_time,$timezone,date_default_timezone_get());

    ////////extrate start & end time
    $start=date("H:i:s",strtotime($start_date_time));
    $end=date("H:i:s",strtotime($end_date_time));

    //////////create dropdown//////////////////////
    $current = strtotime( $start );
    $end = strtotime( $end );
    $dropdown = '';
    $dropdown .= '<option value="" disabled selected="selected" >Start Time</option>';
    while( $current <= $end ) {
        $time = date( 'h:i A', $current );
        if($current==$end)
        {
            $dropdown .= "<option disabled value=\"{$time}\">" . date( 'h.i A', $current ) .'</option>';
        }
        else
        {
            $dropdown .= "<option value=\"{$time}\">" . date( 'h.i A', $current ) .'</option>';
        }

        $current = strtotime( $interval, $current );
    }

    return $dropdown;
}

function check_profile_completed($check = null)
{
    $ci = &get_instance();

    $select_fild = 'profile_completed_id';
    $where_condition = array(
        'matrimony_id' => $ci->session->userdata('customerLoginDetails')['customer_id'], $check => 'Y'
    );
    $table_data = $ci->General_model->get_full_description('matrimony_profile_completed', $select_fild, $where_condition);
    if ($table_data) {
        return '<i class="fa fa-check-circle-o" aria-hidden="true" title="Completed"></i>';
    } else {
        return '<i class="fa fa-circle-o" aria-hidden="true" style="color:#666 !important;" title="Incomplete"></i>';
    }

}
function get_vendor_time_gone($vendor_id=null)
{
    $ci = &get_instance();

    $select_fild = 'timezone';
    $where_condition = array(
        'vendor_id' => $vendor_id
    );
    $table_data = $ci->General_model->get_full_description('astro_session_availability', $select_fild, $where_condition,'astro_availability_id desc');
    if ($table_data) {
        return $table_data->timezone;
    } else {
        return '';
    }
}
function get_total_completed_profile()
{
    $ci = &get_instance();

    $select_fild = '';
    $where_condition = array(
        'matrimony_id' => $ci->session->userdata('customerLoginDetails')['customer_id'],
    );
    $table_data = $ci->General_model->get_full_description('matrimony_profile_completed', $select_fild, $where_condition);
    if ($table_data) {
        $i=0;
        if($table_data->profile_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->physical_appearance_completed=='Y')
        {
            $i= $i+1;

        }
        if($table_data->describe_yourself_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->address_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->horoscope_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->family_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->education_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->profession_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->desired_partner_completed=='Y')
        {
            $i= $i+1;
        }
        if($table_data->update_images_completed=='Y')
        {
            $i= $i+1;
        }
        return $i.'0';
    } else {
        return '';
    }
}

function get_verifications_completed($check = null)
{
    $ci = &get_instance();

    $select_fild = 'verifications_completed_id';
    $where_condition = array(
        'matrimony_id' => $ci->session->userdata('customerLoginDetails')['customer_id'], $check => 'Y'
    );
    $table_data = $ci->General_model->get_full_description('matrimony_verifications_completed', $select_fild, $where_condition);
    if ($table_data) {
        return '<i class="fa fa-check-circle-o" aria-hidden="true" title="Completed"></i>';
    } else {
        return '<i class="fa fa-circle-o" aria-hidden="true" style="color:#666 !important;" title="Incomplete"></i>';
    }

}
function get_total_verifications_completed()
{
    $ci = &get_instance();

    $select_fild = 'verifications_completed_id';
    $where_condition = array(
        'matrimony_id' => $ci->session->userdata('customerLoginDetails')['customer_id'], 'verification_education' => 'Y','verification_employment' => 'Y','verification_address' => 'Y','verification_professional' => 'Y','verification_personal' => 'Y','verification_police' => 'Y','verification_criminal' => 'Y','verification_drug' => 'Y'
    );
    $table_data = $ci->General_model->get_full_description('matrimony_verifications_completed', $select_fild, $where_condition);
    if ($table_data) {
        return '<i class="fa fa-check-circle-o" aria-hidden="true" title="Completed"></i>';
    } else {
        return '<i class="fa fa-circle-o" aria-hidden="true" style="color:#666 !important;" title="Incomplete"></i>';
    }

}
function get_desire_search_religion_dropdown($select_religion = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_religion';
    $fields = 'religion_id,religion_name';
    if($skip_id)
    $where = array('status' => 'E','religion_id!='=>$skip_id);
    else
    $where = array('status' => 'E');
    $order_by = 'order_id asc';
    if($select_religion)
    {
        //var_dump($select_religion);
        $where2=($select_religion);
        $result_data = $ci->General_model->get_desire_search_religion_dropdown($select_religion);
    }

    else
    {
        $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    }

    $dropdown = '<select name="desire_search_religion[]" id="desire_search_religion" class="multiselect-ui form-control bc bc1" multiple="multiple">';

    foreach ($result_data as $row) {
        if ($row->religion_id == $select_religion) {
            $dropdown .= '<option value="' . $row->religion_id . '" selected>' . $row->religion_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->religion_id . '">' . $row->religion_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function get_desire_search_country_dropdown($select_religion = NULL,$skip_id=null)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    if($select_religion)
    {
        //var_dump($select_religion);
        $where2=($select_religion);
        $result_data = $ci->General_model->get_desire_search_country_dropdown($select_religion);
    }
    else
    {
        $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    }

    $dropdown = '<select name="desire_search_country[]" id="desire_search_country" class="multiselect-ui form-control bc cc1" multiple="multiple">';

    foreach ($result_data as $row) {
                    $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';

    }
    $dropdown .= '</select>';
    return $dropdown;
}

function check_chat_count($chat_with_id=null)
{
    $ci = &get_instance();
    ////get first & last day week
    $datestr=date('Y-m-d');
    date_default_timezone_set(date_default_timezone_get());
    $dt = strtotime($datestr);
    $start = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
    $end = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
    ///get total search count/////
    $where_search_month=array(
        'matrimony_id'=>$ci->session->userdata('customerLoginDetails')['customer_id'],
        'chat_date >='=> $start,'chat_date <='=>$end,'deleted'=>'N'
    );
    $search_count=$ci->General_model->getCountRecord('matrimony_chat_count',$where_search_month);
    if($search_count)
    {
        $search_count=$search_count;
    }
    else
    {
        $search_count=1;
    }

    if($search_count)
    {
        $where=array('matrimony_id'=>$ci->session->userdata('customerLoginDetails')['customer_id'],'transaction_type'=>'Membership','current_package'=>'Y');
        $member_current_plan_id=$ci->General_model->get_full_description('profile_package','package_id',$where);
        //get Searches per month of plan///////
        $where=array('package_id'=>$member_current_plan_id->package_id,'feature_id'=>'4','applyed'=>'Y');
        $search_value=$ci->General_model->get_full_description('master_package_value','value',$where);

        if($search_value)
        {

            if($search_value->value!='Unlimited' && $search_value->value!='0')
            {
                // var_dump($search_count,$search_value->value);die;
                if($search_count>=$search_value->value)
                {
                    $data['chat_allow']=0;
$data['placeholder']='Your message was not sent because the message limit was reached. Please try again next week.';
                }
                else
                {
                    $data['chat_allow']=1;
 $data['placeholder']='';
                }
            }
            else
            {
                if($search_value->value=='0')
                {
                    $data['chat_allow']=0;
$data['placeholder']='Messaging is not available in current membership plan. Please upgrade your membership plan';
                }
                if($search_value->value=='Unlimited')
                {
                    $data['chat_allow']=1;
$data['placeholder']='';
                }
            }
        }
        else
        {
            $data['chat_allow']=0;
 $data['placeholder']='Messaging is not available in current membership plan. Please upgrade your membership plan';
        }
    }
    //var_dump($data);die;
    ///////////////////////////////check user already chat//////////////////////////////

    $where_search_month=array(
        'matrimony_id'=>$ci->session->userdata('customerLoginDetails')['customer_id'],
        'chat_with_id'=>$chat_with_id,
        'chat_date >='=> $start,'chat_date <='=>$end
    );
    $already_chat=$ci->General_model->get_full_description('matrimony_chat_count','chat_with_id',$where_search_month);
    if($already_chat)
    {
        $data['already_chat']=1;
    }
    else
    {
        $data['already_chat']=0;
    }

    return $data;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * this function use for get all master list
 * @return mixed this function return all heights  array
 */
function get_master_from_table($table_name, $field_name, $where, $order = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $table_data = $ci->General_model->get_master_description($table_name, $field_name, $where, $order);
    if ($table_data) {
        return $table_data;
    }
    else {
        return 'N/A';
    }
}
function get_height_form_matrimony_id($matrimony_id)
{
    $ci = &get_instance();
    $result_data = $ci->General_model->get_height_form_matrimony_id($matrimony_id);
    return $result_data;
}
function get_country_state_city_form_matrimony_id($matrimony_id)
{
    $ci = &get_instance();
    $result_data = $ci->General_model->get_country_state_city_form_matrimony_id($matrimony_id);
    if($result_data){
        return $result_data;
    }
    else{
        return '';
    }

}
function get_education_form_matrimony_id($matrimony_id)
{
    $ci = &get_instance();
    $result_data = $ci->General_model->get_education_form_matrimony_id($matrimony_id);
    if($result_data){
        return $result_data;
    }
    else{
        return '';
    }
}
function get_income_form_matrimony_id($matrimony_id)
{
    $ci = &get_instance();
    $result_data = $ci->General_model->get_income_form_matrimony_id($matrimony_id);
    if($result_data){
        return $result_data;
    }
    else{
        return '';
    }
}
/**
 * this function use for all form mothertongue dropdown
 * @param null $select_master_mothertongue (already selected mothertongue id )
 * @return string this function return mothertongue dropdown
 */
function get_master_multiple_mothertongue_dropdown($select_master_mothertongue = NULL)
{
    $ci = &get_instance();
    $table='master_mothertongue';
    $fields='mothertongue_id,mothertongue_name';
    $where=array('status'=>'E');
    $order_by='order_id asc';
    $result_data = $ci->General_model->get_listing_details($table,$fields,$where,$order_by);
    $dropdown = '<select name="mothertongue[]" id="mothertongue" class="selectpicker show-tick form-control" data-live-search="true" multiple="multiple">';
    $dropdown .= '<option value="" disabled>Please Select Language</option>';
    if($select_master_mothertongue)
    {
        foreach ($result_data as $row) {
            if (in_array($row->mothertongue_id,json_decode($select_master_mothertongue))) {
                $dropdown .= '<option value="' . $row->mothertongue_id . '" selected>' . $row->mothertongue_name . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';
            }
        }
    }
    else
    {
        foreach ($result_data as $row) {
            $dropdown .= '<option value="' . $row->mothertongue_id . '">' . $row->mothertongue_name . '</option>';
        }
    }
    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function use for all form religion dropdown
 * @param null $select_religion (already selected religion id )
 * @return string this function return religion dropdown
 */
function get_master_religion_dropdown_admin($select_religion = NULL)
{
    $ci = &get_instance();
    $table = 'master_religion';
    $fields = 'religion_id,religion_name';
    $where = array('status' => 'E');
    $order_by = 'order_id asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="religion_id" id="religion_id" class="form-control">';

    foreach ($result_data as $row) {
        if ($row->religion_id == $select_religion) {
            $dropdown .= '<option value="' . $row->religion_id . '" selected>' . $row->religion_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->religion_id . '">' . $row->religion_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function use for all form country dropdown
 * @param null $select_country (already selected country id )
 * @return string this function return country dropdown
 */
function get_admin_country_dropdown($select_country = NULL)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="country_id" id="country_id" class="form-control country_id">';

    foreach ($result_data as $row) {
        if ($row->country_id == $select_country) {
            $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
/**
 * this function create state dropdown
 * @param null $select_country (pass last select country id)
 * @param null $select_state (already select state id)
 * @return string (this function return state dropdown)
 */
function get_admin_state_dropdown($select_country = NULL, $select_state = NULL)
{
    $ci = &get_instance();
    $table = 'master_state';
    $fields = 'state_id,state_name';
    $where = array('country_id' => $select_country, 'status' => 'E');
    $order_by = 'state_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="state_id" id="state_id" class="form-control">';

    foreach ($result_data as $row) {
        if ($row->state_id == $select_state) {
            $dropdown .= '<option value="' . $row->state_id . '" selected>' . $row->state_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->state_id . '">' . $row->state_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}

/**
 * this function create city dropdown
 * @param null $select_state (pass last select state id)
 * @param null $select_city (already select city id)
 * @return string (this function return city dropdown)
 */
function get_admin_city_dropdown($select_state = NULL, $select_city = NULL)
{
    $ci = &get_instance();
    $table = 'master_city';
    $fields = 'city_id,city_name';
    $where = array('state_id' => $select_state, 'status' => 'E');
    $order_by = 'city_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="city_id" id="city_id" class="form-control">';
    foreach ($result_data as $row) {
        if ($row->city_id == $select_city) {
            $dropdown .= '<option value="' . $row->city_id . '" selected>' . $row->city_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->city_id . '">' . $row->city_name . '</option>';
        }
    }
    $dropdown .= '</select>';
    return $dropdown;
}
function getStatusId($status)
{
    $ci = &get_instance();
    $table = 'master_status';
    $field='status_id';
    $where = array('status'=>$status);
    $result_data = $ci->General_model->get_listing_details($table,$field,$where);
    return  $result_data[0]->status_id;
}
function cart_check_coupon_code($coupon_id=null,$package_id=null,$package_type,$package_value)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->cart_already_coupon_code_check($coupon_id,$package_id,$package_type,$package_value);
    if ($table_data) {
        return $table_data;
    } else {
        return '';
    }
}

function check_LS_FS_exist_in_plan($planId=null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    //FS plan included in master plan
    $select_fild = '';
    $where_condition3 = array(
        'package_id' => $planId,
        'feature_id'=>30,
        'applyed'=>'Y'
    );
    $fs_package_exist = $ci->General_model->get_full_description('master_package_value', $select_fild, $where_condition3);
    if($fs_package_exist)
    {
        ////1.check user login or not
        if($ci->session->userdata('customerLoginDetails')['login_customer_type']=='M')
        {
            /////check user city in tier (1 / 2/ 3)

            $where_city = array('matrimony_id' => $ci->session->userdata('customerLoginDetails')['customer_id'],'permanent_address'=>'Yes');
            $user_city= $ci->General_model->get_full_description('matrimony_address', 'city_id', $where_city);
            if($user_city)
            {
                ////get tier city data///////
                $where_city = array('city_id' => $user_city->city_id);
                $tier= $ci->General_model->get_full_description('master_tier_city', 'tier', $where_city);
                if($tier)
                {
                    $where_tier = array('sub_type' => 'tier'.$tier->tier);
                    $ls_price= $ci->General_model->get_full_description('master_package', 'month3_amount', $where_tier);
                    return $ls_price->month3_amount;
                }
                else
                {
                    ///////set tier3 price
                    $where_tier = array('package_id' => 8);
                    $ls_price= $ci->General_model->get_full_description('master_package', 'month3_amount', $where_tier);
                    return $ls_price->month3_amount;
                }

            }
            else
            {
                ///////set tier3 price
                $where_tier = array('package_id' => 8);
                $ls_price= $ci->General_model->get_full_description('master_package', 'month3_amount', $where_tier);
                return $ls_price->month3_amount;
            }

        }
        else
        {
            ///////set tier3 price
            $where_tier = array('package_id' => 8);
            $ls_price= $ci->General_model->get_full_description('master_package', 'month3_amount', $where_tier);
            return $ls_price->month3_amount;
        }

    }
    // OR LS plan included in master plan
    $select_fild = '';
    $where_condition2 = array(
        'package_id' => $planId,
        'feature_id'=>29,
        'applyed'=>'Y'
    );
    $ls_package_exist = $ci->General_model->get_full_description('master_package_value', $select_fild, $where_condition2);
    if($ls_package_exist)
    {
        $where_ls = array('package_id' => 7);
        $ls_price= $ci->General_model->get_full_description('master_package', 'month3_amount', $where_ls);
        return $ls_price->month3_amount;
    }

    if($fs_package_exist=='' && $ls_package_exist=='')
    {
        return 0;
    }

}

function get_tax_rate()
{
    return 1.05;
}

function get_ls_sub_plan($matrimony_id)
{

    $ci = &get_instance();
    $user_id=$ci->session->userdata('customerLoginDetails')['customer_id'];

    /////check already verify request pending/completed
    $plan='';
    $myArray='';
    $where_condition = array(
        'post_matrimony_id'=>$user_id,
        'response_matrimony_id' => $matrimony_id,
        'service'=>'LS',
        ''
    );
    $check_requested_already = $ci->General_model->get_listing_details('matrimony_verifications_request', 'verifications_request_id', $where_condition);
    if($check_requested_already)
    {   foreach($check_requested_already as $raw1)
    {
        $where = array('verifications_request_id'=>$raw1->verifications_request_id);
        $already_plan = $ci->General_model->get_listing_details('matrimony_verifications_request_sub_type','package_id',$where);
        $array_plan_apply = json_encode($already_plan);
        $array_plan_apply = json_decode($array_plan_apply , true);
        $plan[]=(array_column($array_plan_apply,'package_id'));
    }
    }

    if($plan)
    {
        $tmpArr = array();
        foreach ($plan as $sub) {
            $tmpArr[] = implode(',', $sub);
        }
        $plan = implode(',', $tmpArr);
        $myArray = explode(',', $plan);
    }


    $table = 'master_package';
    $field='package_id,package_name,month3_amount';
    $where = array('package_type'=>'matriverify','sub_type'=>'sub','status'=>'E');
    $result_data = $ci->General_model->get_listing_details($table,$field,$where);
    //var_dump($result_data);
    $dropdown = '';
    if($result_data) {
        $dropdown .= '<li><label class="control control--checkbox"><input class="ls_plan_id plan_id_first" type="checkbox" name="ls_sub_plan_id[]" value="0">All<div class="control__indicator"></div></label></li>';
        if ($myArray) {
            foreach ($result_data as $row) {
                if (in_array($row->package_id, ($myArray))) {
                    $dropdown .= '<li><label class="control control--checkbox"><span>'.$row->package_name.' '.$row->month3_amount.'</span>(Requested Already)<div class="control__indicator"></div></label></li>';
                } else {
                    $dropdown .= '<li><label class="control control--checkbox"><input class="ls_plan_id" type="checkbox" name="ls_sub_plan_id[]" value="' . $row->package_id . '"> ' . $row->package_name . ' ' . $row->month3_amount . '<div class="control__indicator"></div></label></li>';
                }
            }
        } else {
            foreach ($result_data as $row) {
                $dropdown .= '<li><label class="control control--checkbox"><input class="ls_plan_id" type="checkbox" name="ls_sub_plan_id[]" value="' . $row->package_id . '"> ' . $row->package_name . ' ' . $row->month3_amount . '<div class="control__indicator"></div></label></li>';
            }
        }
    }
    return $dropdown;

}
function get_venue_single_slider_image($venue_id)
{


    $ci = &get_instance();
    $ci->load->model('General_model');
    $where_condition = array('venue_id' => $venue_id);
    $table_data = $ci->General_model->get_full_description('matrimony_venue_slider', 'venue_slider_images', $where_condition);
    if ($table_data) {
        // var_dump($table_data->venue_slider_images);
        return base_url() . 'upload_dir/venue_images/' . $table_data->venue_slider_images;
    } else {
        return base_url() . 'assets/front/images/users/default.png';
    }

}
function get_venue_slider_image($venue_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $venue_images='';
    $where_condition = array('venue_id' => $venue_id);
    $table_data = $ci->General_model->get_venue_slider_image('matrimony_venue_slider', 'venue_slider_images', $where_condition);
    if ($table_data) {
        foreach($table_data as $row){
            $venue_images[]= base_url() . 'upload_dir/venue_images/' . $row->venue_slider_images;


        }

        return $venue_images;

    } else {
        return '';
    }

}



function get_venue_block_image($venue_id, $venue_block_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $venue_block_images='';
    $where_condition = array('venue_id' => $venue_id,'venue_block_id' => $venue_block_id );
    $table_data = $ci->General_model->get_venue_slider_image('matrimony_venue_block_images', 'block_image', $where_condition);
    if ($table_data) {
        foreach($table_data as $row){
            $venue_block_images[]= base_url().'upload_dir/venue_images/venue_block_images/'.$row->block_image;
        }

        return $venue_block_images;

    }
    else {
        return '';
    }
}

function table_chan($table,$field,$order)
{
    $ci = &get_instance();
    $table = $table;
    $fields = $field;
    $where = array();
    $order_by = $order;
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);

    foreach ($result_data as $row) {
        $where = array($fields=>$row->$fields);
        $data=array($fields=>$row->$fields+2);
        $ci->General_model->update_details($table, $data, $where);
    }


}
/**
 *  this function use for get select field for table
 * @param $table_name ( name of table_name)
 * @param $field_name (name of field)
 * @param $where ( where condition)
 * @return string (return name of field)
 */
function get_multiple_fields($table_name, $field_name, $where, $order = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_full_description($table_name, $field_name, $where, $order);
    if ($table_data) {
        return $table_data;
    } else {
        return '';
    }

}
function get_multiple_data_adds($table_name, $field_name, $where,$where2,$where3, $poition = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_listing_details_advertise($table_name, $field_name, $where,$where2,$where3, $poition);
    if ($table_data) {
        return $table_data;
    } else {
        return '';
    }

}
/**
 *  this function use for get select field for table
 * @param $table_name ( name of table_name)
 * @param $field_name (name of field)
 * @param $where ( where condition)
 * @return string (return name of field)
 */
function get_multiple_data($table_name, $field_name, $where, $order = null)
{
    $ci = &get_instance();
    $ci->load->model('General_model');

    $table_data = $ci->General_model->get_listing_details($table_name, $field_name, $where, $order);
    if ($table_data) {
        return $table_data;
    } else {
        return '';
    }

}

function get_cs_profile_image($cs_id)
{
    $ci = &get_instance();
    $ci->load->model('General_model');
    $where_condition = array('admin_id' => $cs_id);
    $table_data = $ci->General_model->get_full_description('admin_users', 'admin_profile_image', $where_condition);
    if ($table_data->admin_profile_image) {
        return base_url() . 'upload_dir/admin_image/' . $table_data->admin_profile_image;
    } else {
        return base_url() . 'assets/front/images/users/default.png';
    }
}
function get_member_current_package($id=null)
{   $ci = &get_instance();
    $select_fild = 'package_id,sub_type,created_date,expiry_date';
    $where_member_package = array(
        'matrimony_id' => $id,'current_package'=>'Y','transaction_type'=>'Membership'
    );
    $member_package = $ci->General_model->get_full_description('profile_package', $select_fild, $where_member_package);

    if($member_package)
    {
        return $member_package->sub_type;
    }
    else
    {
        return '';
    }
}
function get_search_top_astro()
{
    $ci = &get_instance();
    $table = 'vendor';
    $fields = 'vendor_id';
    $where = array('type' => 'A', 'status' => 'E');
    $order_by = 'vendor_id desc';
    $result_data = $ci->General_model->get_full_description($table, $fields, $where, $order_by);
    if($result_data)
    {
        return $result_data->vendor_id;
    }
    else
    {
        return '';
    }

}

function get_brows_country_dropdown($select_country = NULL)
{

    $ci = &get_instance();

    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="country[]" id="brows_country_id" class="form-control">';
    $dropdown .= '<option value="">Please Select Country</option>';
    foreach ($result_data as $row) {
        if ($row->country_name == $select_country) {
            $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
function get_brows_state_dropdown($select_country = NULL, $select_state = NULL)
{
    $ci = &get_instance();
    $where = array('country_name' => $select_country, 'status' => 'E');
    $result = $ci->General_model->get_full_description('master_country', 'country_id', $where);
    if($result)
        $select_country=$result->country_id;
    else
        $select_country='108';

    $where = array('city_name' => $select_state, 'status' => 'E');
    $result1 = $ci->General_model->get_full_description('master_city', 'state_id', $where);
    if($result1)
        $select_state=$result1->state_id;
    else
        $select_state='4421';

    $table = 'master_state';
    $fields = 'state_id,state_name';
    $where = array('country_id' => $select_country, 'status' => 'E');
    $order_by = 'state_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="state[]" id="brows_desire_state" class="form-control" >';

    foreach ($result_data as $row) {
        if ($row->state_id == $select_state) {
            $dropdown .= '<option value="' . $row->state_id . '" selected>' . $row->state_name . '</option>';
        } else {
            $dropdown .= '<option value="' . $row->state_id . '">' . $row->state_name . '</option>';
        }

    }
    $dropdown .= '</select>';
    return $dropdown;
}
function code_to_country( $code )
{
    $code = strtoupper($code);
    $country_phone_code = array("BD" => "880", "BE" => "32", "BF" => "226",
        "BG" => "359", "BA" => "387", "BB" => "+1-246",
        "WF" => "681", "BL" => "590", "BM" => "+1-441",
        "BN" => "673", "BO" => "591", "BH" => "973",
        "BI" => "257", "BJ" => "229", "BT" => "975",
        "JM" => "+1-876", "BV" => "", "BW" => "267",
        "WS" => "685", "BQ" => "599", "BR" => "55",
        "BS" => "+1-242", "JE" => "+44-1534", "BY" => "375", "BZ" => "501",
        "RU" => "7", "RW" => "250", "RS" => "381", "TL" => "670", "RE" => "262",
        "TM" => "993", "TJ" => "992", "RO" => "40", "TK" => "690", "GW" => "245",
        "GU" => "+1-671", "GT" => "502", "GS" => "", "GR" => "30", "GQ" => "240",
        "GP" => "590", "JP" => "81", "GY" => "592", "GG" => "+44-1481", "GF" => "594",
        "GE" => "995", "GD" => "+1-473", "GB" => "44", "GA" => "241", "SV" => "503",
        "GN" => "224", "GM" => "220", "GL" => "299", "GI" => "350", "GH" => "233",
        "OM" => "968", "TN" => "216", "JO" => "962", "HR" => "385", "HT" => "509",
        "HU" => "36", "HK" => "852", "HN" => "504", "HM" => " ", "VE" => "58",
        "PR" => "+1-787 and 1-939", "PS" => "970", "PW" => "680", "PT" => "351",
        "SJ" => "47", "PY" => "595", "IQ" => "964", "PA" => "507", "PF" => "689",
        "PG" => "675", "PE" => "51", "PK" => "92", "PH" => "63", "PN" => "870",
        "PL" => "48", "PM" => "508", "ZM" => "260", "EH" => "212", "EE" => "372",
        "EG" => "20", "ZA" => "27", "EC" => "593", "IT" => "39", "VN" => "84",
        "SB" => "677", "ET" => "251", "SO" => "252", "ZW" => "263", "SA" => "966",
        "ES" => "34", "ER" => "291", "ME" => "382", "MD" => "373", "MG" => "261",
        "MF" => "590", "MA" => "212", "MC" => "377", "UZ" => "998", "MM" => "95",
        "ML" => "223", "MO" => "853", "MN" => "976", "MH" => "692", "MK" => "389",
        "MU" => "230", "MT" => "356", "MW" => "265", "MV" => "960", "MQ" => "596",
        "MP" => "+1-670", "MS" => "+1-664", "MR" => "222", "IM" => "+44-1624",
        "UG" => "256", "TZ" => "255", "MY" => "60", "MX" => "52", "IL" => "972",
        "FR" => "33", "IO" => "246", "SH" => "290", "FI" => "358", "FJ" => "679",
        "FK" => "500", "FM" => "691", "FO" => "298", "NI" => "505", "NL" => "31",
        "NO" => "47", "NA" => "264", "VU" => "678", "NC" => "687", "NE" => "227",
        "NF" => "672", "NG" => "234", "NZ" => "64", "NP" => "977", "NR" => "674",
        "NU" => "683", "CK" => "682", "XK" => "", "CI" => "225", "CH" => "41",
        "CO" => "57", "CN" => "86", "CM" => "237", "CL" => "56", "CC" => "61",
        "CA" => "1", "CG" => "242", "CF" => "236", "CD" => "243", "CZ" => "420",
        "CY" => "357", "CX" => "61", "CR" => "506", "CW" => "599", "CV" => "238",
        "CU" => "53", "SZ" => "268", "SY" => "963", "SX" => "599", "KG" => "996",
        "KE" => "254", "SS" => "211", "SR" => "597", "KI" => "686", "KH" => "855",
        "KN" => "+1-869", "KM" => "269", "ST" => "239", "SK" => "421", "KR" => "82",
        "SI" => "386", "KP" => "850", "KW" => "965", "SN" => "221", "SM" => "378",
        "SL" => "232", "SC" => "248", "KZ" => "7", "KY" => "+1-345", "SG" => "65",
        "SE" => "46", "SD" => "249", "DO" => "+1-809 and 1-829", "DM" => "+1-767",
        "DJ" => "253", "DK" => "45", "VG" => "+1-284", "DE" => "49", "YE" => "967",
        "DZ" => "213", "US" => "1", "UY" => "598", "YT" => "262", "UM" => "1",
        "LB" => "961", "LC" => "+1-758", "LA" => "856", "TV" => "688", "TW" => "886",
        "TT" => "+1-868", "TR" => "90", "LK" => "94", "LI" => "423", "LV" => "371",
        "TO" => "676", "LT" => "370", "LU" => "352", "LR" => "231", "LS" => "266",
        "TH" => "66", "TF" => "", "TG" => "228", "TD" => "235", "TC" => "+1-649",
        "LY" => "218", "VA" => "379", "VC" => "+1-784", "AE" => "971", "AD" => "376",
        "AG" => "+1-268", "AF" => "93", "AI" => "+1-264", "VI" => "+1-340", "IS" => "354",
        "IR" => "98", "AM" => "374", "AL" => "355", "AO" => "244", "AQ" => "", "AS" => "+1-684",
        "AR" => "54", "AU" => "61", "AT" => "43", "AW" => "297", "IN" => "91", "AX" => "+358-18",
        "AZ" => "994", "IE" => "353", "ID" => "62", "UA" => "380", "QA" => "974", "MZ" => "258");
    if($code){
        return $country_phone_code[$code];
    }
    else
        {
            return '';
        }

}

function active_user_count()
{
    $ci = &get_instance();

    $result_data = $ci->General_model->count_active_user();
    if($result_data)
    return $result_data->total_active;
    else
        return 0;
}

function register_user_count()
{
    $ci = &get_instance();
    $where=array('user_type'=>'M');
    $result_data = $ci->General_model->get_total_count_record('profile_login',$where);
    //var_dump($result_data);
    if($result_data)
        return $result_data;
    else
        return 0;
}
function verified_user_count()
{
    $ci = &get_instance();
    $where=array('user_type'=>'M','email_verified'=>'Y');
    $result_data = $ci->General_model->get_total_count_record('profile_login',$where);
    if($result_data)
        return $result_data;
    else
        return 0;
}
function get_advertise_country_dropdown($select_country = NULL)
{
    $ci = &get_instance();
    $table = 'master_country';
    $fields = 'country_id,country_name';
    $where = array('status' => 'E');
    $order_by = 'country_name asc';
    $result_data = $ci->General_model->get_listing_details($table, $fields, $where, $order_by);
    $dropdown = '<select name="country_id[]" id="country_id" class="selectpicker show-tick form-control advertise_country" data-live-search="true" multiple="multiple" >';
    $dropdown .= '<option value="0">All</option>';

    if($select_country)
    {
        foreach ($result_data as $row) {
            if (in_array($row->country_id, json_decode($select_country))) {

                $dropdown .= '<option value="' . $row->country_id . '" selected>' . $row->country_name . '</option>';
            } else {
                $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';
            }

        }
    }
    else
    {
        foreach ($result_data as $row) {
                $dropdown .= '<option value="' . $row->country_id . '">' . $row->country_name . '</option>';


        }
    }

    $dropdown .= '</select>';
    return $dropdown;
}

function get_user_hide_dropdown()
{   $ci = &get_instance();
    $where = array('transaction_type' => 'Membership', 'current_package' => 'Y', 'matrimony_id'=>$ci->session->userdata('customerLoginDetails')['customer_id']);
    $result = $ci->General_model->get_full_description('profile_package', 'expiry_date', $where);

    if($result)
    {
        $date1 = new DateTime(date('Y-m-d'));
        $date2 = new DateTime($result->expiry_date);

        $diff = $date2->diff($date1)->format("%a");
        $dropdown = '';
        if($dropdown>6)
            $k=1;
        else
            $k=1;
        for($i=1;$i<=30;$i++) {
                $dropdown .= '<option value="+' . $i . ' days">' . $i . ' Days</option>';
        }
        $dropdown .= '</select>';
        return $dropdown;
    }


}