<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends CI_Controller {
	function __construct(){
		parent::__construct();
		checkSession();
	}
	public function index(){
        if($this->session->userdata('auth')['role_id']!=1)
        redirect(DOMAIN.'dashboard');

		$data['agent'] = $this->db->select('user.*,roles.role_name')->join('roles','roles.id=user.role_id','left')->get('user')->result();
       //echo $this->db->last_query();
//       var_dump(json_encode($data));die; 
		$this->load->view('profile-list',$data);
    }
    function active_inactive_ajax()
    {
        $status=$this->input->post('status');
        $id=$this->input->post('id');
        
        $update=$this->CoreModel->update_details('user',$s=array('status'=>$status),$where=array('id'=>$id));
        if($update)
        echo json_encode($d=array('status'=>1));
        else
        echo json_encode($d=array('status'=>0));
    }
    function delete_user($id=null)
    {
     if($id)
     {
         $this->db->delete('user',$where=array('id'=>$id));
     }
     redirect(DOMAIN.'profile');
    }
    function checkemail()
    {
        $d=array('status'=>'0');
        $id=$this->input->post('id');
        $email=$this->input->post('email');
        if($id)
        {
            $old_email=$this->CoreModel->get_full_description('user','email',$where=array('id'=>$id));
            if($old_email->email!=$email)
            {
                $email_check=$this->CoreModel->get_full_description('user','email',$where=array('email'=>$email));
                if($email_check)
                {
                    $d=array('status'=>1);
                }
            
            }
        }
        else
        {
            $email_check=$this->CoreModel->get_full_description('user','email',$where=array('email'=>$email));
            if($email_check)
            {
                $d=array('status'=>1);
            }
        }

        echo json_encode($d);

    } 
    function check_unique_id()
    {
        $d=array('status'=>'0');
        $id=$this->input->post('id');
        $unique_id=$this->input->post('unique_id');
        if($id)
        {
            $old_unique_id=$this->CoreModel->get_full_description('user','unique_id',$where=array('id'=>$id));
            if($old_unique_id->unique_id!=$unique_id)
            {
                $unique_id_check=$this->CoreModel->get_full_description('user','unique_id',$where=array('unique_id'=>$unique_id));
                if($unique_id_check)
                {
                    $d=array('status'=>1);
                }
            
            }
        }
        else
        {
            $unique_id_check=$this->CoreModel->get_full_description('user','unique_id',$where=array('unique_id'=>$unique_id));
                if($unique_id_check)
                {
                    $d=array('status'=>1);
                }
        }

        echo json_encode($d);

    } 
	public function add_edit_user($id=null)
	{

    	$data['listing']='';
		$data['location']=$this->CoreModel->get_listing_details('location','','');
		$data['state']=$this->CoreModel->get_listing_details('state','','');
		$data['cluster']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('user','',$where=array('id'=>$id));
		$cluster= $this->CoreModel->get_full_description('cluster','',$where=array('cluster_name'=>$data['listing']->cluster));
        
        if($cluster)
       { $data['cluster']=$this->CoreModel->get_listing_details('cluster','',$where=array('state_id'=>$cluster->state_id));}
	  }
	  if($this->input->post('submit'))
	  {
       
        //$this->form_validation->set_rules('email', 'email', 'trim|required|is_unique[user.email]');
        //$this->form_validation->set_rules('unique_id', 'unique_id', 'trim|required|is_unique[user.email]');
       
        //if ($this->form_validation->run() == TRUE) {
        $where=array('id'=>$this->input->post('id'));
        if($_POST['password']){
        $_POST['password_hash'] = password_hash($_POST['password'],PASSWORD_DEFAULT);
        }
		unset($_POST['state_id']);
		unset($_POST['password']);
		unset($_POST['conf_pass']);
        unset($_POST['submit']);
        //var_dump($_POST);die;
		if($this->input->post('id')) {
            unset($_POST['id']);
        	$this->CoreModel->update_details('user',$_POST,$where);
		}
		else
		{
            unset($_POST['id']);
		$this->CoreModel->insert_details('user',$_POST);
        }
        if($this->session->userdata('auth')['role_id']==1)         
        {
            
            redirect(DOMAIN.'profile');
        }
        else
        {
            $this->session->set_flashdata('msg', 'Profile Updated Successfully');
            redirect(DOMAIN.'profile/add_edit_user/'.$this->session->userdata('auth')['id']);
        }
       }
       else
       {

       }
	  //}
	  
	  $this->load->view('add_edit_user',$data);
	}
public function get_cluster()
{
	$state_id=$this->input->post('state_id');
	$data=$this->CoreModel->get_listing_details('cluster','',$where=array('state_id'=>$state_id));
	$data2=$this->CoreModel->get_listing_details('location','','');
	
	$d=array('status'=>1,'data'=>$data,'data2'=>$data2);
	echo json_encode($d);
}
	public function logout(){
		logout();
	}
	public function change_password(){
		$this->load->view('change-password');
	}
	public function replace_password(){
		if(!empty($_POST)){
			if($this->input->post('nPassword') == $this->input->post('rnPassword')){
				$data = $this->CoreModel->checkEmail();
				if(varifyPassword($this->input->post('oPassword'),$data[0]->passCode)){
					if($this->CoreModel->setPassword())
						$this->session->set_flashdata('flash','Password Update Successfully.');
					else
						$this->session->set_flashdata('flash','Password Update Failed.');
				}else
					$this->session->set_flashdata('flash','Incorrect Old Password.');
			}else
				$this->session->set_flashdata('flash','New Password Columns Not Matched.');
		}
		redirect(DOMAIN.'profile/change_password');
	}


	


    public function send_email($id=null)
    {
        $details=$this->db->select('title,fName,mName,lName,emailAddress,alise,mobile,InternalPOSCode,aadharCard')->where('id',$id)->get('credentials')->row();
        if($details) {
            $user_name = $details->title .' '.$details->fName . ' ' . $details->mName . ' ' . $details->lName;
            $login_email = $details->emailAddress;
            
            $email_variable['name'] = $user_name;
            $email_variable['InternalPOSCode'] = $details->InternalPOSCode;
            $email_variable['aadharCard'] = $details->aadharCard;
            $email_variable['email'] = $details->emailAddress;
            $email_variable['pass'] = $details->alise;
            $domain=explode('_admin',DOMAIN);
            $email_variable['url'] = $domain[0];

            $email_variable['title_email_template'] = 'Autraining Result';

            $email = $login_email;
            $emailToList = array();
            $emailCCList = array('');
            $emailBCCList = array();
            $emailToList[] = $email;
            $messageforsend = $this->load->view("result_email", $email_variable, TRUE);
            //var_dump($messageforsend);die;
            $subject = 'Autraining Result';
            sendmail($email, $emailCCList, $emailBCCList, $messageforsend, $subject);
            //==========
            //$this->load->library('email');
           // $this->email->from('noreply@goforsys.com', 'Autraining');
           // $this->email->to($login_email);
           // $this->email->cc('kamalps008@gmail.com');
           // $this->email->subject($subject);
           // $this->email->message($messageforsend);
           // $this->email->set_mailtype("html");
           // $this->email->send();
            // Always set content-type when sending HTML email
           // $headers = "MIME-Version: 1.0" . "\r\n";
           // $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            //mail($forgot_email,$subject,$message,$headers);
            // $status=mail('kamal@coretechies.org' ,$subject,$messageforsend,$headers);
           // $this->email->clear();

            $this->db->where('userId', $id)->update('result_meta', $update = array('email_sent' => 'Y'));
        }
        return redirect(DOMAIN.'profile/agent_pass_fail');



    }
    
    public function download_csv($type=null)
    {
        $type= $this->uri->segment(3);
        //var_dump($_POST);die;
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        if($type=='all')
        {
        if(count($_POST['checked'])>0)
        {
            $checked=implode(',',$_POST['checked']);
            //var_dump($checked);die;
            //date = m-d-yyyy
            $query = $this->db->query("select credentials.aadharCard as Adhaar,credentials.fName as POSPFName,credentials.mName as POSPMName,credentials.lName as POSPLName, DATE_FORMAT(credentials.dob,'%m/%d/%Y') as DoB,credentials.city as City,credentials.pinCode as Pin, DATE_FORMAT(result_meta.recordTimestamp,'%m/%d/%Y') as AppointmentDate,credentials.emailAddress as Email,credentials.mobile as Mobile,
            if(result_meta.recordTimestamp is not null, 'Y', 'N') as status,credentials.InternalPOSCode, credentials.panCard,credentials.bankname,credentials.accountno,credentials.ifsc, CONCAT(`credentials`.`house`,',',`credentials`.`street`,',',`credentials`.`city`,',',`credentials`.`district`,',',`credentials`.`state`) as Address from credentials 
            left join result_meta ON credentials.id=result_meta.userId where credentials.id IN ($checked)");
        }
        else
        {
           $query = $this->db->query("select credentials.aadharCard as Adhaar,credentials.fName as POSPFName,credentials.mName as POSPMName,credentials.lName as POSPLName, DATE_FORMAT(credentials.dob,'%m/%d/%Y') as DoB,credentials.city as City,credentials.pinCode as Pin, DATE_FORMAT(result_meta.recordTimestamp,'%m/%d/%Y') as AppointmentDate,credentials.emailAddress as Email,credentials.mobile as Mobile,
            if(result_meta.recordTimestamp is not null, 'Y', 'N') as status,credentials.InternalPOSCode, credentials.panCard,credentials.bankname,credentials.accountno,credentials.ifsc, CONCAT(`credentials`.`house`,',',`credentials`.`street`,',',`credentials`.`city`,',',`credentials`.`district`,',',`credentials`.`state`) as Address from credentials 
            left join result_meta ON credentials.id=result_meta.userId");
        }
//var_dump($query);die;

        }
        if($type=='pass')
        {
            $query = $this->db->query("select credentials.aadharCard as Adhaar,credentials.fName as POSPFName,credentials.mName as POSPMName,credentials.lName as POSPLName, DATE_FORMAT(credentials.dob,'%m/%d/%Y') as DoB,credentials.city as City,credentials.pinCode as Pin, DATE_FORMAT(result_meta.recordTimestamp,'%m/%d/%Y') as AppointmentDate,credentials.emailAddress as Email,credentials.mobile as Mobile,
if(result_meta.recordTimestamp is not null, 'Y', 'N') as status,credentials.InternalPOSCode, credentials.panCard,credentials.bankname,credentials.accountno,credentials.ifsc, CONCAT(`credentials`.`house`,',',`credentials`.`street`,',',`credentials`.`city`,',',`credentials`.`district`,',',`credentials`.`state`) as Address from credentials 
join result_meta ON credentials.id=result_meta.userId where result_meta.obtainPercentage>='60' ");
//var_dump($query);die;

        }
        if($type=='fail')
        {
            $query = $this->db->query("select credentials.aadharCard as Adhaar,credentials.fName as POSPFName,credentials.mName as POSPMName,credentials.lName as POSPLName, DATE_FORMAT(credentials.dob,'%m/%d/%Y') as DoB,credentials.city as City,credentials.pinCode as Pin, DATE_FORMAT(result_meta.recordTimestamp,'%m/%d/%Y') as AppointmentDate,credentials.emailAddress as Email,credentials.mobile as Mobile,
if(result_meta.recordTimestamp is not null, 'Y', 'N') as status,credentials.InternalPOSCode, credentials.panCard,credentials.bankname,credentials.accountno,credentials.ifsc, CONCAT(`credentials`.`house`,',',`credentials`.`street`,',',`credentials`.`city`,',',`credentials`.`district`,',',`credentials`.`state`) as Address from credentials 
left join result_meta ON credentials.id=result_meta.userId where  result_meta.obtainPercentage IS NULL OR result_meta.obtainPercentage < '60'");
//var_dump($query);die;

        }
        $delimiter = ",";
        $newline = "\r\n";
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download('CSV_Report.csv', $data);

    }
    
}
