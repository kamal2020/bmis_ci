<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct(){
		parent::__construct();
		checkSession();
		if($this->session->userdata('auth')['role_id']!=1)
    redirect(DOMAIN.'dashboard');
	}
	
	public function country()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('countries','','');
		$this->load->view('country',$data);
	}
	public function state()
	{
		$data['listing'] = $this->CoreModel->get_state_listing_details();
		$this->load->view('state',$data);
	}
	public function city()
	{
		$data['listing'] = $this->CoreModel->get_city_listing_details();
		$this->load->view('city',$data);
	}
	public function cluster()
	{
		$data['listing'] = $this->CoreModel->get_cluster_listing_details();
		$this->load->view('cluster',$data);
	}

	public function product()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('product_category','','');
		$this->load->view('product',$data);
	}
	public function add_edit_cluster($id=null)
	{
	  $data['listing']='';
	  $data['state']=$this->CoreModel->get_listing_details('state','','');
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('cluster','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('cluster',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('cluster',$_POST);
		}
		redirect(DOMAIN.'master/cluster');
	  }
	  
	  $this->load->view('add_edit_cluster',$data);

	}
	public function add_edit_city($id=null)
	{
	  $data['listing']='';
	  $data['state']=$this->CoreModel->get_listing_details('state','','');
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('city','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('city',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('city',$_POST);
		}
		redirect(DOMAIN.'master/city');
	  }
	  
	  $this->load->view('add_edit_city',$data);

	}
	public function add_edit_state($id=null)
	{
	  $data['listing']='';
	  $data['country']=$this->CoreModel->get_listing_details('countries','','');
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('state','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('state',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('state',$_POST);
		}
		redirect(DOMAIN.'master/state');
	  }
	  
	  $this->load->view('add_edit_state',$data);

	}
	public function add_edit_country($id=null)
	{ 
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('countries','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('countries',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('countries',$_POST);
		}
		redirect(DOMAIN.'master/country');
	  }
	  
	  $this->load->view('add_edit_country',$data);

	}
	public function add_edit_product($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('product_category','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('product_category',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('product_category',$_POST);
		}
		redirect(DOMAIN.'master/product');
	  }
	  
	  $this->load->view('add_edit_product',$data);

	}
	public function vehicle_category()
	{
		$data['listing'] = $this->CoreModel->get_vehicle_category_listing_details();
		$this->load->view('vehicle_category',$data);
	}
	public function add_edit_vehicle_category($id=null)
	{
	  $data['listing']='';
	  $data['product']=$this->CoreModel->get_listing_details('product_category','','');
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('vehicle_category','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('vehicle_category',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('vehicle_category',$_POST);
		}
		redirect(DOMAIN.'master/vehicle_category');
	  }
	  
	  $this->load->view('add_edit_vehicle_category',$data);

	}
	public function hpn()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('hpn_table','','');
		$this->load->view('hpn',$data);
	}
	public function add_edit_hpn($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('hpn_table','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('hpn_table',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('hpn_table',$_POST);
		}
		redirect(DOMAIN.'master/hpn');
	  }
	  
	  $this->load->view('add_edit_hpn',$data);

	}
	public function policy_type()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('policy_type','','');
		$this->load->view('policy_type',$data);
	}
	public function add_edit_policy_type($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('policy_type','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('policy_type',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('policy_type',$_POST);
		}
		redirect(DOMAIN.'master/policy_type');
	  }
	  
	  $this->load->view('add_edit_policy_type',$data);

	}
	public function ncb()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('ncb_tbl','','');
		$this->load->view('ncb',$data);
	}
	public function add_edit_ncb($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('ncb_tbl','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('ncb_tbl',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('ncb_tbl',$_POST);
		}
		redirect(DOMAIN.'master/ncb');
	  }
	  
	  $this->load->view('add_edit_ncb',$data);

	}
	public function verticle()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('verticle','','');
		$this->load->view('verticle',$data);
	}
	public function add_edit_verticle($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('verticle','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('verticle',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('verticle',$_POST);
		}
		redirect(DOMAIN.'master/verticle');
	  }
	  
	  $this->load->view('add_edit_verticle',$data);

	}

	public function business_type()
	{
		$data['listing'] = $this->CoreModel->get_business_type_listing_details();
		$this->load->view('business_type',$data);
	}
	public function add_edit_business_type($id=null)
	{
	  $data['listing']='';
	  $data['verticle']=$this->CoreModel->get_listing_details('verticle','','');
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('business_type','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('business_type',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('business_type',$_POST);
		}
		redirect(DOMAIN.'master/business_type');
	  }
	  
	  $this->load->view('add_edit_business_type',$data);

	}

	public function sourcing_category()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('sourcing_category','','');
		$this->load->view('sourcing_category',$data);
	}
	public function add_edit_sourcing_category($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('sourcing_category','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('sourcing_category',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('sourcing_category',$_POST);
		}
		redirect(DOMAIN.'master/sourcing_category');
	  }
	  
	  $this->load->view('add_edit_sourcing_category',$data);

	}

	public function source_type()
	{
		$data['listing'] = $this->CoreModel->get_source_type_listing_details();
		$this->load->view('source_type',$data);
	}
	public function add_edit_source_type($id=null)
	{
	  $data['listing']='';
	  $data['sourcing_category']=$this->CoreModel->get_listing_details('sourcing_category','','');
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('source_type','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('source_type',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('source_type',$_POST);
		}
		redirect(DOMAIN.'master/source_type');
	  }
	  
	  $this->load->view('add_edit_source_type',$data);

	}

	public function insurance_company()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('insurance_company','','');
		$this->load->view('insurance_company',$data);
	}
	public function add_edit_insurance_company($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('insurance_company','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('insurance_company',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('insurance_company',$_POST);
		}
		redirect(DOMAIN.'master/insurance_company');
	  }
	  
	  $this->load->view('add_edit_insurance_company',$data);

	}
	public function location()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('location','','');
		$this->load->view('location',$data);
	}
	public function add_edit_location($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('location','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('location',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('location',$_POST);
		}
		redirect(DOMAIN.'master/location');
	  }
	  
	  $this->load->view('add_edit_location',$data);

	}
	public function payment_mode()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('payment_received_mode','','');
		$this->load->view('payment_mode',$data);
	}
	public function add_edit_payment_mode($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('payment_received_mode','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('payment_received_mode',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('payment_received_mode',$_POST);
		}
		redirect(DOMAIN.'master/payment_mode');
	  }
	  
	  $this->load->view('add_edit_payment_mode',$data);

	}
	public function source_code_name()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('source_code_source_name','','');
		$this->load->view('source_code_name',$data);
	}
	public function add_edit_source_code_name($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('source_code_source_name','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('source_code_source_name',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('source_code_source_name',$_POST);
		}
		redirect(DOMAIN.'master/source_code_name');
	  }
	  
	  $this->load->view('add_edit_source_code_name',$data);

	}
	
	public function insurance_exec_code_name()
	{
		$data['listing'] = $this->CoreModel->get_listing_details('insurance_exec_code','','');
		$this->load->view('insurance_exec_code_name',$data);
	}
	public function add_edit_insurance_exec_code_name($id=null)
	{
	  $data['listing']='';
	  
	  if($id)
	  {
		$data['listing'] = $this->CoreModel->get_full_description('insurance_exec_code','',$where=array('id'=>$id));
	  }
	  if($this->input->post('submit'))
	  {
		if($this->input->post('id')) {
		$where=array('id'=>$this->input->post('id'));
		unset($_POST['id']);
		unset($_POST['submit']);
		$this->CoreModel->update_details('insurance_exec_code',$_POST,$where);
		}
		else
		{
			unset($_POST['id']);
			unset($_POST['submit']);
			$this->CoreModel->insert_details('insurance_exec_code',$_POST);
		}
		redirect(DOMAIN.'master/insurance_exec_code_name');
	  }
	  
	  $this->load->view('add_edit_insurance_exec_code_name',$data);

	}


}
