<?php 
//echo APPPATH.'sessionfoldername';
$this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      BMIS
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">BMIS</li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
          <div class="col-md-12">
            <h3 class="box-title">BMIS List</h3>
            <span class="pull-right"><a href='<?=DOMAIN?>bmis/add_edit_bmis' class='btn btn-primary'>Add New BMIS</a></span>
          </div>

          <div class="col-md-12 tt">

		  
      <?php if (!empty($bmis)) { ?>
        <form action="" method="POST" id="exportForm">
		  
          		<div class="drop_buttons pull-right">
		   			<div class="btn-group" role="group">
							<div class="btn-group" role="group">
								<button type="button" id="w0-cols" class="btn btn-default dropdown-toggle" title="Select columns to export" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" header="<li class=&quot;dropdown-header&quot;>Select Columns</li><li class=&quot;kv-divider&quot;></li>"><i class="glyphicon glyphicon-list"></i>  <span class="caret"></span></button>

								<ul style="max-height: 200px; overflow:auto;" id="w0-cols-list" class="dropdown-menu kv-checkbox-list" role="menu" aria-labelledby="w0-cols">
        							<li>
        								<div class="checkbox">
            							<label>
												    <input type="checkbox" id="masterCheckBox">                
												    <span class="kv-toggle-all">Toggle All</span>       
                          </label>
        								</div>
    								</li>
    								<li class="divider"></li>
									<?php  
										$array = get_object_vars($bmis[0]); 
                    $properties = array_keys($array);
										foreach ($properties as $key => $value) {
                      
                      if ($value != "id" && $value != "user_id" && $value != "sr_no"){
									?>
                      <li>
                        <div class="checkbox" >
                          <label>
                          <?php

                            $field = "";

                            if ($value == "source_name_1_insurance_executive_name")
                            {
                              $field = "Insurance Executive Name";
                            }
                            else if ($value == "source_code_1_insurance_executive_code")
                            {
                              $field = "Insurance Executive Code";
                            }
                            else if ($value == "username")
                            {
                              $field = "Agent Name";
                            }
                            else 
                            {
                              $field = ucwords(str_replace("_", " ", $value));
                            }
                            
                            $val = "";
                            if ($value == "username")
                            {
                              $val = "user.username";
                            }
                            else {
                              $val = "bmis.".$value;
                            }
                            ?>
                            
                            <input type="checkbox" name="fields[]" class="listBox" value="<?=$val.' as `'.$field.'`'?>">
                            <?=$field?>
                          </label>
                        </div>
                      </li>
										<?php } } ?>
								</ul>

                

							</div>

							<div class="btn-group">
								<button id="w2" class="btn btn-default dropdown-toggle" title="Export data in selected format" data-toggle="dropdown">
								 	<i class="glyphicon glyphicon-export"></i>
								 	<span class="caret"></span> 
								</button> 
                <input type="hidden" name="date_range_export" value="<?=$this->session->flashdata('dates')?>" >
								<ul id="w3" class="dropdown-menu">
									<li title="Microsoft Excel 2007+ (xlsx)"><a value='submit' id="export" type="submit" name='export' class="export-full-excel2007" href="#" data-format="Excel2007" tabindex="-1"><i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel 2007+</a></li>
								</ul>
							</div>
					</div>
			  	</div>


			
      </form>
      <?php } ?>
        <form action="" method="POST">
        <div class="col-md-3 nopading" style="align: center">
            <label>Select Date Range</label>
            <input type="text" class="form-control" id="dateRange" name="date_range">
        </div>
        <div class="col-md-3 nopading">
            <input type="submit" class="btn btn-primary search-btn" value="search">
        </div>
       </div>
      </form>
        </div>
	 
          <!-- /.box-header -->
          <div class="box-body table-responsive">
          
            <table class="table table-bordered table-striped table-condenced">

              <thead>
              <tr>
                <th>#</th>
                <th>Receive From</th>
                <th>Agent Name</th>
                <th>Entery Date</th>
                <th>Location</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              <?php $i=0; foreach($bmis as $key=>$value) {?>
                <tr>
                  <td><?=++$i?></td>
                  <td><?=$value->receive_from?></td>
                  <td><?=$value->username?></td>
                  <td><?=date('F d, Y',strtotime($value->entry_date))?></td>
                  <td><?=$value->location?></td>
                  
                  <td>
                  <a href="<?=DOMAIN.'bmis/bmis_view/'.$value->id?>" class="ico"><span class="glyphicon glyphicon-eye-open"></span></a>
                  <a href="<?=DOMAIN.'bmis/add_edit_bmis/'.$value->id?>" class="ico"><span class="glyphicon glyphicon-pencil"></span></a>
                  <a href="<?=DOMAIN.'bmis/delete_bmis/'.$value->id?>" class="ico"><span class="glyphicon glyphicon-trash"></span></a>
                  
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
<script>
  $('.del').click(function(){
    if (window.confirm('Are you sure you want to delete this agent.?'))
    {return true;}
    else{return false;}
  });
  $('.make_active').click(function(){
    var status= parseInt($(this).attr("data-status"));
    var id= $(this).attr("data-id");
    if (window.confirm('Are you sure you want to '+((status)? 'Active' : 'Inactive')+' this agent.?'))
    {
      
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>profile/active_inactive_ajax",
            data: {status:status,id:id},
            dataType: "json",
            success: function(d) {
                if (d.status == 1)
                {
                  
                  if(status==0)
                  {$('#'+id).attr('data-status','1');$('#s_'+id).removeClass('fa fa-toggle-on').addClass('fa fa-toggle-off');}
                  else
                  {$('#'+id).attr('data-status','0');$('#s_'+id).removeClass('fa fa-toggle-off').addClass('fa fa-toggle-on');}

                }
                
            }
        });
    }
    else
    {
      return false;
    }
  });

  $("#masterCheckBox").click(function () {
    $(".listBox").prop('checked', $(this).prop('checked'));
  });

  $('#export').click(function() {
    $('#exportForm').submit();
  });

  $(function() {
    $('.listBox, #masterCheckBox').prop('checked', true);
    $('#dateRange').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
  });
</script>