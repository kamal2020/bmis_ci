<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" href="<?=ASSETS?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>BMIS</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$today_total?></h3>

              <p>Total Today</p>
            </div>
            <div class="icon">
              
            </div>
            
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$month_total?></h3>

              <p>Total Month</p>
            </div>
            <div class="icon">
              
            </div>
            
          </div>
        </div>
        </div>
        <div class="row">
        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>

<script src="<?=ASSETS?>plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?=ASSETS?>dist/js/pages/dashboard2.js"></script>
<script src="<?=ASSETS?>dist/js/demo.js"></script>

<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	title:{
		text: "Month Rewiew"
	},
	axisY: {
		title: "BMIS"
	},
	data: [{        
		type: "column",  
		showInLegend: true, 
		legendMarkerColor: "grey",
		legendText: "",
		dataPoints:      
		<?=$chart;?>
	}]
});
chart.render();

}
</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
