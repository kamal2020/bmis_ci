<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      Users
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Users</li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
            <h3 class="box-title">User List</h3>
            <span class="pull-right"><a href='<?=DOMAIN?>profile/add_edit_user' class='btn btn-primary'>Add New User</a></span>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-condenced">
              <thead>
              <tr>
                <th>Serial #</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Location</th>
                <th>State</th>
                <th>Cluster</th>
                <th>Date</th>
                <th>Role</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              <?php $i=0; foreach($agent as $key=>$value) {?>
                <tr>
                  <td><?=++$i?></td>
                  <td><?=$value->username?></td>
                  <td><?=$value->email?></td>
                  <td><?=$value->contact_no?></td>
                  <td><?=$value->location?></td>
                  <td><?=$value->state?></td>
                  <td><?=$value->cluster?></td>
                  <td><?=date('d-m-Y',strtotime($value->created_date))?></td>
                  <td><?php if($value->role_id=='1'){ echo 'Admin';} else { echo 'Agent';}?></td>
                  <td>
                  <a href="<?=DOMAIN.'profile/add_edit_user/'.$value->id?>" class="ico"><span class="glyphicon glyphicon-pencil"></span></a>
                  <a href="<?=DOMAIN.'profile/delete_user/'.$value->id?>" class="ico"><span class="glyphicon glyphicon-trash"></span></a>
                  <?php if($value->status==1){ ?>
                  <a id='<?=$value->id?>' data-id="<?=$value->id?>" data-status="0" class="ico make_active"><span id='s_<?=$value->id?>' class="fa fa-toggle-on"></span></a>
                  <?php } else { ?>
                  <a id='<?=$value->id?>' data-id="<?=$value->id?>" data-status="1" class="ico make_active"><span id='s_<?=$value->id?>' class="fa fa-toggle-off"></span></a>
                  <?php } ?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
<script>
  $('.del').click(function(){
    if (window.confirm('Are you sure you want to delete this agent.?'))
    {return true;}
    else{return false;}
  });
  $('.make_active').click(function(){
    var status= parseInt($(this).attr("data-status"));
    var id= $(this).attr("data-id");
    if (window.confirm('Are you sure you want to '+((status)? 'Active' : 'Inactive')+' this agent.?'))
    {
      
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>profile/active_inactive_ajax",
            data: {status:status,id:id},
            dataType: "json",
            success: function(d) {
                if (d.status == 1)
                {
                  
                  if(status==0)
                  {$('#'+id).attr('data-status','1');$('#s_'+id).removeClass('fa fa-toggle-on').addClass('fa fa-toggle-off');}
                  else
                  {$('#'+id).attr('data-status','0');$('#s_'+id).removeClass('fa fa-toggle-off').addClass('fa fa-toggle-on');}

                }
                
            }
        });
    }
    else
    {
      return false;
    }
  });
</script>