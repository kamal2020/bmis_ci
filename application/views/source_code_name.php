<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header -->
  <section class="content-header">
      <h1>
      Source Code - Source Name
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Source Code - Source Name</li>
      </ol>
    </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        
        <div class="box">
       
          <div class="box-header">
            <h3 class="box-title">Source Code - Source Name List</h3>
            <span class="pull-right"><a href='<?=DOMAIN?>master/add_edit_source_code_name' class='btn btn-primary'>Add New Country</a></span>            
          </div>
          
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table class="table table-bordered table-striped table-condenced">
              <thead>
              <tr>
                <th>Serial #</th>
                <th>Source Code</th>
                <th>Source Name</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              <?php $i=0; foreach($listing as $key=>$value) {?>
                <tr>
                  <td><?=++$i?></td>
                  <td><?=$value->source_code?></td>
                  <td><?=$value->source_name?></td>
                  <td><a href="<?=DOMAIN.'master/add_edit_source_code_name/'.$value->id?>" class="btn btn-warning btn-xs">Edit</a></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
         
          <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
  </div>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
