<style>
.upper 
{
    text-transform:capitalize;
}
</style>
<?php $this->load->view('include/header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Page Header Vehicle Category, Product,Brokerage Premuim,Verticle,Sourcing Category,Hpn,business_type-->
  	<section class="content-header">
    	<h1>User</h1>
      	<ol class="breadcrumb">
        	<li><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        	<li class="active">Create Bmis</li>
      	</ol>
    </section>

  	<section class="content">
    	<div class="row">
      		<div class="col-md-12">
        
        		<div class="box">
       
          <!-- <div class="box-header">
            <h3 class="box-title">User</h3>
                        
          </div> -->
          
          <!-- /.box-header -->
          			<div class="box-body table-responsive">
          				<form role="form" method="post" action="" id="profile">
          					<input type='hidden' name='id' value='<?php if($listing){ echo $listing->id; } ?>' >
          					<div class="form-group col-md-3">
                      			<label>Receive From</label>
                      			<input type="text" class="form-control upper" name="receive_from" value="<?=$this->session->userdata('auth')['location']?>" placeholder="" readonly>
          					</div>
          					<!-- <div class="form-group col-md-3">
                      			<label>Sr no</label>
                      			<input type="text" class="form-control" name="sr_no" value="<?php if($listing){ echo $listing->sr_no; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Unique ID</label>
                      			<input type="text" class="form-control" name="unique_id" value="<?php if($listing){ echo $listing->unique_id; } ?>" placeholder="" required>
          					</div> -->
         					<div class="form-group col-md-3">
                      			<label>Month</label>
                      			<input readonly type="text" class="form-control upper" name="month" value="<?php if($listing){ echo $listing->month; } else { echo date('F');} ?>" placeholder="" required>
         					</div>
							<div class="form-group col-md-3">
                      			<label>Entry Date</label>
                      			<input <?php if($this->session->userdata('auth')['role_id']!='1'){ echo 'readonly'; }?> type="text" class="form-control date" name="entry_date" value="<?php if($listing){ echo $listing->entry_date; } else { echo date('d-M-Y');} ?>" placeholder="" required>
        					</div>
							<div class="form-group col-md-3">
                      			<label>State</label>
								  <input type="hidden" name="state" id="state" value="<?php if($listing){ echo $listing->id; } ?>">
                      			<select value="Rajasthan" class="form-control" name="state_id" id="state_id" required>
									<option value="">Select...</option>  
									<?php for ($i = 0; $i < count($state); $i++) { ?>
									<option value="<?= $state[$i] -> id ?>" 
										<?php if($listing){ if(trim($listing->state)==$state[$i]->state_name) { echo 'selected';} } else { if ($state[$i] -> state_name == "Rajasthan"){echo 'selected';} } ?>><?= $state[$i]-> state_name ?></option>		
									<?php } ?>
								</select>
        					</div>
							<div class="form-group col-md-3">
                      			<label>Cluster</label>
                      			<select class="form-control" name="cluster" id="cluster" required>
									<option value="">Cluster</option>
								</select>
        					</div>
							<div class="form-group col-md-3">
                      			<label>Location</label>
                      			<select class="form-control" name="location" id="" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($location); $i++) { ?>
									<option 
										value="<?= $location[$i] -> location ?>" <?php if($listing){ if($listing->location==$location[$i] -> location) { echo 'selected';} } ?>><?= $location[$i] -> location ?></option>		
									<?php } ?>
								</select>
        					</div>
							<div class="form-group col-md-3">
                      			<label>Customer Name</label>
                      			<input type="text" class="form-control upper" name="customer_name" value="<?php if($listing){ echo $listing->customer_name; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Contact No</label>
                      			<input type="number" class="form-control" name="contact_no" id="contact_no" value="<?php if($listing){ echo $listing->contact_no; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Customer Address</label>
                      			<input type="text" class="form-control upper" name="customer_address" value="<?php if($listing){ echo $listing->customer_address; } ?>" placeholder="" required>
          					</div>
							  <div class="form-group col-md-3">
                      			<label>Customer State</label>
								  <input type="hidden" name="customer_state" id="customer_state" value="<?php if($listing){ echo $listing->id; } ?>">
                      			<select class="form-control" name="customer_state_id" id="customer_state_id" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($state); $i++) { ?>
									<option 
										value="<?= $state[$i] -> id ?>" 
										<?php if($listing){ if(trim($listing->customer_state)==$state[$i] -> state_name) { echo 'selected';} } else { if ($state[$i] -> state_name == "Rajasthan"){echo 'selected';} }?>
										><?= $state[$i] -> state_name ?></option>		
									<?php } ?>
								</select>
          					</div>
							<div class="form-group col-md-3">
                      			<label>Customer City</label>
                      			<select class="form-control" name="customer_city" id="customer_city" required>
									<option value="">Customer City</option>
								</select>
         					</div>
          					<div class="form-group col-md-3">
                      			<label>Make</label>
                      			<input type="text" class="form-control upper" name="make" value="<?php if($listing){ echo $listing->make; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Model</label>
                      			<input type="text" class="form-control upper" name="model" value="<?php if($listing){ echo $listing->model; } ?>" placeholder="" required>
          					</div>
         					<div class="form-group col-md-3">
                      			<label>Reg No</label>
                      			<input id="regno" type="text" class="form-control upper" name="reg_no" value="<?php if($listing){ echo $listing->reg_no; } ?>" placeholder="" required>
         					</div>
							<div class="form-group col-md-3">
                      			<label>Engine No</label>
                      			<input type="text" class="form-control upper" name="engine_no" value="<?php if($listing){ echo $listing->engine_no; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Chasis No</label>
                      			<input type="text" class="form-control upper" name="chasis_no" value="<?php if($listing){ echo $listing->chasis_no; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Vehicle Category</label>
								<input type='hidden' name="vehicle_category" id="vehicle_category" value="<?php if($listing){ echo $listing->vehicle_category; } ?>">
                      			<select class="form-control" name="vehicle_category_id" id="vehicle_category_id" required>
								  <option value="">Select...</option>
								  <?php for ($i = 0; $i < count($vehicle_category); $i++) { ?>
									<option 
										value="<?= $vehicle_category[$i] -> id ?>" 
										<?php if($listing){ if(trim($listing->vehicle_category)==$vehicle_category[$i] -> vehicle_category) { echo 'selected';} } ?>
									><?= $vehicle_category[$i] -> vehicle_category ?></option>		
									<?php } ?>
								</select>
          					</div>
							  <div class="form-group col-md-3">
                      			<label>Product</label>
                      			<input id="product" type="text" class="form-control" value="<?php if($listing){ echo $listing->product; } ?>" name="product" readonly>
          					</div>
         					<div class="form-group col-md-3">
                      			<label>Insurance Company</label>
                      			<select class="form-control" name="insurance_company" value="" placeholder="" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($insurance_company); $i++) { ?>
									<option 
										value="<?= $insurance_company[$i] -> insurance_company ?>" 
										<?php if($listing){ if($listing->insurance_company==$insurance_company[$i] -> insurance_company) { echo 'selected';} } ?>
									><?= $insurance_company[$i] -> insurance_company ?></option>		
									<?php } ?>
								</select>
         					</div>
							<div class="form-group col-md-3">
                      			<label>Company Office</label>
                      			<input type="text" class="form-control upper" name="company_office" value="<?php if($listing){ echo $listing->company_office; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Hpn</label>
                      			<select type="text" class="form-control" name="hpn" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($hpn); $i++) { ?>
									<option 
										value="<?= $hpn[$i] -> hpn ?>" 
										<?php if($listing){ if($listing->hpn==$hpn[$i] -> hpn) { echo 'selected';} } ?>
									><?= $hpn[$i] -> hpn ?></option>		
									<?php } ?>
								</select>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Manufacturing year</label>
                      			<select class="form-control" name="manufacturing_year" required>
										<option value="">Select...</option>
										<?php $prev = 0;
										 while (date("Y",strtotime("-".$prev." year")) != 1979){ ?>
											<option value="<?=date("Y",strtotime("-".$prev." year"))?>"
											<?php if($listing){ if($listing->manufacturing_year==date("Y",strtotime("-".$prev." year"))) { echo 'selected';} } ?>
											><?=date("Y",strtotime("-".$prev." year"))?></option>
										<?php $prev++; } ?>
								</select>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Issue Date</label>
                      			<input type="text" class="form-control date" name="issue_date" value="<?php if($listing){ echo $listing->issue_date; } ?>" placeholder="" required>
          					</div>
 							<div class="form-group col-md-3">
                      			<label>Risk Start Date</label>
                      			<input type="text"  class="form-control date" name="risk_start_date" value="<?php if($listing){ echo $listing->risk_start_date; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Risk End Date</label>
                      			<input type="text" id="bmis-risk_end_date" class="form-control date" name="risk_end_date" value="<?php if($listing){ echo $listing->risk_end_date; } ?>">
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Policy Type</label>
                      			<select class="form-control" name="policy_type" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($policy_type); $i++) { ?>
									<option 
										value="<?= $policy_type[$i] -> policy_type ?>" 
										<?php if($listing){ if($listing->policy_type==$policy_type[$i] -> policy_type) { echo 'selected';} } ?>
									><?= $policy_type[$i] -> policy_type ?></option>		
									<?php } ?>
								</select>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Cover Note Number</label>
                      			<input type="text" class="form-control upper" name="cover_note_number" value="<?php if($listing){ echo $listing->cover_note_number; } ?>" placeholder="" required>
          					</div>
 							<div class="form-group col-md-3">
                      			<label>Policy Number</label>
                      			<input type="text" class="form-control upper" name="policy_number" value="<?php if($listing){ echo $listing->policy_number; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Discount %</label>
                      			<input type="number" class="form-control" name="disscount" value="<?php if($listing){ echo $listing->disscount; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Current Ncb</label>
                      			<select class="form-control" name="current_ncb" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($ncb); $i++) { ?>
									<option 
										value="<?= $ncb[$i] -> ncb ?>" 
										<?php if($listing){ if($listing->current_ncb==$ncb[$i] -> ncb) { echo 'selected';} } ?>
									><?= $ncb[$i] -> ncb ?></option>		
									<?php } ?>
								</select>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Sum Insured</label>
                      			<input type="number" class="form-control" name="sum_insured" value="<?php if($listing){ echo $listing->sum_insured; } ?>" placeholder="" required>
          					</div>
 							<div class="form-group col-md-3">
                      			<label>Pacakge Od</label>
                      			<input type="number" class="form-control" name="pacakge_od" id="pacakge_od" value="<?php if($listing){ echo $listing->pacakge_od; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Liability Tp</label>
                      			<input type="number" class="form-control" name="liability_tp" id="liability_tp" value="<?php if($listing){ echo $listing->liability_tp; } ?>" placeholder="" required>
          					</div>
							<div class="form-group col-md-3">
                      			<label>Brokerage Premuim</label>
                      			<input type="number" class="form-control" name="brokerage_premium" id="brokerage_premium" value="<?php if($listing){ echo $listing->brokerage_premium; } ?>" placeholder="" readonly>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Gross Premium</label>
                      			<input type="number" readonly class="form-control" name="gross_premium" id="gross_premium" value="<?php if($listing){ echo $listing->gross_premium; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Service Tax</label>
                      			<input type="number" readonly class="form-control" name="service_tax" id="service_tax" value="<?php if($listing){ echo $listing->service_tax; } ?>" placeholder="" required>
          					</div>
 							<div class="form-group col-md-3">
                      			<label>Total Premium</label>
                      			<input type="number" readonly class="form-control" name="total_premium" id="total_premium" value="<?php if($listing){ echo $listing->total_premium; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Upfront</label>
                      			<input type="number" class="form-control" name="upfront" value="<?php if($listing){ echo $listing->upfront; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Payout Percent</label>
                      			<input type="number" class="form-control" name="payout_percent" value="<?php if($listing){ echo $listing->payout_percent; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Payment Received Mode</label>
                      			<select class="form-control" name="payment_received_mode" required>
									<option value=""></option>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($payment_received_mode); $i++) { ?>
									<option 
										value="<?= $payment_received_mode[$i] -> payment_mode ?>" 
										<?php if($listing){ if($listing->payment_received_mode==$payment_received_mode[$i] -> payment_mode) { echo 'selected';} } ?>
									><?= $payment_received_mode[$i] -> payment_mode ?></option>		
									<?php } ?>
								</select>
          					</div>
 							<div class="form-group col-md-3">
                      			<label>Payment Received Mode Detail</label>
                      			<input type="text" class="form-control" name="payment_received_mode_detail" value="<?php if($listing){ echo $listing->payment_received_mode_detail; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Payment Received Amount</label>
                      			<input type="number" class="form-control" name="payment_received_amount" value="<?php if($listing){ echo $listing->payment_received_amount; } ?>" placeholder="" required>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Business Type</label>
								<input type="hidden" name="business_type" id="business_type" value="<?php if($listing){ echo $listing->business_type; } ?>" required>
                      			<select class="form-control" name="business_type_id" id="business_type_id">
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($business_type); $i++) { ?>
									<option 
										value="<?= $business_type[$i] -> id ?>" 
										<?php if($listing){ if(trim($listing->business_type)==$business_type[$i] -> business_type) { echo 'selected';} } ?>
										><?= $business_type[$i] -> business_type ?></option>		
									<?php } ?>
								</select>
          					</div>
							<div class="form-group col-md-3">
                      			<label>Verticle</label>
                      			<input id="verticle" type="text" class="form-control" name="verticle" value="<?php if($listing){ echo $listing->verticle; } ?>" placeholder="" readonly>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Source Type</label>
                                <input type="hidden" name="source_type" id="source_type" value="<?php if($listing){ echo $listing->source_type; } ?>">
                      			<select id="sourceType_id" class="form-control" name="source_type_id" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($source_type); $i++) { ?>
									<option 
										value="<?= $source_type[$i] -> id ?>" 
										<?php if($listing){ if(trim($listing->source_type)==$source_type[$i] -> source_type) { echo 'selected';} } ?>
									><?= $source_type[$i] -> source_type ?></option>		
									<?php } ?>
								</select>
          					</div>
							  <div class="form-group col-md-3">
                      			<label>Sourcing Category</label>
                      			<input id="sourcingCategory" type="text" class="form-control" name="sourcing_category" value="<?php if($listing){ echo $listing->sourcing_category; } ?>" placeholder="" readonly>
          					</div>


 							<div class="form-group col-md-3">
                      			<label>Source Code</label>
                      			<select id="sourceCode" class="form-control" name="source_code" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($source); $i++) { ?>
									<option 
										value="<?= $source[$i] -> source_code ?>" 
										<?php if($listing){ if(trim($listing->source_code)==$source[$i] -> source_code) { echo 'selected';} } ?>
									><?= $source[$i] -> source_code ?></option>		
									<?php } ?>
								</select>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Source Name</label>
                      			<input id="sourceName" type="text" class="form-control" name="source_name" value="<?php if($listing){ echo $listing->source_name; } ?>" readonly>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Insurance Executive Code</label>
                      			<select id="execCode" class="form-control" name="source_code_1_insurance_executive_code" id="execCode" required>
									<option value="">Select...</option>
									<?php for ($i = 0; $i < count($insurance_exec_code); $i++) { ?>
									<option 
										value="<?= $insurance_exec_code[$i] -> insurance_exec_code ?>" 
										<?php if($listing){ if(trim($listing->source_code_1_insurance_executive_code)==$insurance_exec_code[$i] -> insurance_exec_code) { echo 'selected';} } ?>
									><?= $insurance_exec_code[$i] -> insurance_exec_code ?></option>		
									<?php } ?>
								</select>
          					</div>
          					<div class="form-group col-md-3">
                      			<label>Insurance Executive Name</label>
                      			<input id="execName" readonly type="text" class="form-control" value="<?php if($listing){ echo $listing->source_name_1_insurance_executive_name; } ?>" name="source_name_1_insurance_executive_name">
          					</div>


							<div class="form-group col-md-6">
                      			<label>Remark</label>
                      			<textarea type="text" class="form-control" name="remark"><?php if($listing){ echo $listing->remark; } ?></textarea>
          					</div>

							<div class="box-footer">
							<div class="form-group col-md-12">
							<button type="submit" name='submit' value='submit' class="btn btn-primary">Submit</button>
							<button class="btn btn-default backLink">Go Back</button>

							</div>
		
        				</form>

          			</div>
         
          <!-- /.box-body -->
        		</div>
      		</div>
    	</div>
  	</section>
</div>
</div>
<script>
 $("#submit").click(function() {
      var contact_no= $('#contact_no').val();

     if(contact_no.length<10 || contact_no.length>15)
     {
      alert('Contact number must be 10-15 Degit.');
      $('#contact_no').focus();
      return false;
     }
     if($('#conf_pass').val()!='')
     {
       if($('#conf_pass').val()!=$('#password').val())
       {
         alert('Confirm Password not match');
         return false;
       }

     }
    });
	$("#contact_no").change(function() {
      var contact_no= $('#contact_no').val();

      if(contact_no.length<10 || contact_no.length>15)
      {
        alert('Contact number must be 10-15 Degit.');
        $('#contact_no').focus();
      }
      
      
    });
	var	sel_cluster;
	var	sel_city;
	var	sel_state;
	<?php if($listing){ ?>
	sel_cluster="<?=$listing->cluster?>";
	sel_city="<?=$listing->customer_city?>";
	
	<?php } ?>

	$(document).ready(function(){
	$("#state_id").change();
	$("#customer_state_id").change();
	});
	$("#pacakge_od").change(function(){
	var od=parseInt($("#pacakge_od").val());
	var tp=parseInt($("#liability_tp").val());
	if(tp){
	if(od==0)
	$("#brokerage_premium").val(tp);
	else
	$("#brokerage_premium").val(od);

    $('#gross_premium').val(od+tp);
    
	var tax=((od+tp)*0.18).toFixed(2);
	
	$('#service_tax').val(tax);
    $('#total_premium').val(od+tp+tax);
}

    });
	$("#liability_tp").change(function(){
	var od=parseInt($("#pacakge_od").val());
	var tp=parseInt($("#liability_tp").val());
	if(od==0)
	$("#brokerage_premium").val(tp);
	else
	$("#brokerage_premium").val(od);

    $('#gross_premium').val(od+tp);
    
	var tax=((od+tp)*0.18).toFixed(2);
	
	$('#service_tax').val(tax);
    $('#total_premium').val(od+tp+tax);


    });

	$("#state_id").change(function() {
      var state_id= $('#state_id').val();
      var thisvalue = $(this).find("option:selected").text();
      $('#state').val(thisvalue);
      //alert(thisvalue);
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>bmis/get_cluster",
            data: {state_id : state_id},
            dataType: "json",
            success: function(d) {
                if ((parseInt(d.msg_status) == 0))
                {
                  var option_string_default = '';
                  var option_string = '<option value="">Please Select</option>';
                }
                else {
                    var option_string_default = '';
                    var option_string = '<option value="">Please Select</option>';
                    
                    var selected='';
                    $.each(d.data, function(i, e) {
                        selected='';
                        if(sel_cluster==e.cluster_name){selected='selected'; }
                        option_string += '<option '+ selected + ' ';
                        option_string += 'value="' + e.cluster_name	+ '" ';
                        option_string += '>';
                        option_string += e.cluster_name;
                        option_string += '</option>';
                    });
                    
                    }
                $("#cluster").html('').append(option_string_default + ' ' + option_string);
                
            }
        });
    });

	$("#customer_state_id").change(function() {
      var state_id= $('#customer_state_id').val();
	  
      var thisvalue = $(this).find("option:selected").text();
      $('#customer_state').val(thisvalue);
      //alert(thisvalue);
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>bmis/get_city",
            data: {state_id : state_id},
            dataType: "json",
            success: function(d) {
                if ((parseInt(d.msg_status) == 0))
                {
                  var option_string_default = '';
                  var option_string = '<option value="">Please Select</option>';
                }
                else {
                    var option_string_default = '';
                    var option_string = '<option value="">Please Select</option>';
                    
                    var selected='';
                    $.each(d.data, function(i, e) {
                        selected='';
                        if(sel_city==e.city_name){selected='selected'; }
                        option_string += '<option '+ selected + ' ';
                        option_string += 'value="' + e.city_name	+ '" ';
                        option_string += '>';
                        option_string += e.city_name;
                        option_string += '</option>';
                    });
                    
                    }
                $("#customer_city").html('').append(option_string_default + ' ' + option_string);
                
            }
        });
    });
	$('#vehicle_category_id').change(function() {
		var thisvalue = $(this).find("option:selected").text();
        $('#vehicle_category').val(thisvalue);
		$.ajax({
			url: '<?=base_url().'bmis/get_product'?>',
			data: {productId: $(this).val()},
			type: 'POST',
			dataType: 'json',
			success: function(data)
			{
				$('#product').val(data.pro);
			}
		});
	});

	$('#business_type_id').change(function() {
		var thisvalue = $(this).find("option:selected").text();
        $('#business_type').val(thisvalue);
		$.ajax({
			url: '<?=base_url().'bmis/get_verticle'?>',
			data: {verticleId: $(this).val()},
			type: 'POST',
			dataType: 'json',
			success: function (data) 
			{
				$('#verticle').val(data.verticle);
			}
		});
	});

	$('#sourceType_id').change(function() {
		var thisvalue = $(this).find("option:selected").text();
        $('#source_type').val(thisvalue);
		$.ajax({
			url: '<?=base_url().'bmis/get_sourcing_category'?>',
			data: {catId: $(this).val()},
			type: 'POST',
			dataType: 'json',
			success: function (data) 
			{
				$('#sourcingCategory').val(data.category);
			}
		});
	});

	$('#sourceCode').change(function() {
		$.ajax({
			url: '<?=base_url().'bmis/get_source_name'?>',
			data: {sourceCode: $(this).val()},
			type: 'POST',
			dataType: 'json',
			success: function (data) 
			{
				$('#sourceName').val(data.name);
			}
		});
	});

	$('#execCode').change(function() {
		$.ajax({
			url: '<?=base_url().'bmis/get_exec_name'?>',
			data: {execCode: $(this).val()},
			type: 'POST',
			dataType: 'json',
			success: function (data) 
			{
				$('#execName').val(data.name);
			}
		});
	});
	$( function() {
    $( ".date" ).datepicker({ dateFormat: 'dd-M-yy' });
	
  } );

  $('#regno').focusout(function() {
	var pattern = /([a-z]{2}-\d{2}[ ,][a-z0-9]{1,2}[a-z]-\d{4})|([a-z]{2} \d{2}[ ,][a-z0-9]{1,2}[a-z] \d{4})/;
         
		var regno = $(this).val();	  
		if(regno){
		$(this).val('');
		var regArray = regno.split("");
		regArray.splice(2, 0, "-");
		regArray.splice(5, 0, "-");
		regArray.splice(-4, 0, "-");


		
		var newRegno = regArray.toString().replace(/,/g,"").toUpperCase();
		// if (newRegno.match(pattern)){
            $(this).val(newRegno);
        //  }
        //  else {
        //    alert();
    //    }
		}
  });

  $('#regno').focus(function() {
	  var no = $(this).val();
	  var newRegno = no.replace(/-/g,"");
	  $(this).val(newRegno);
  });



</script>
<!-- /.content-wrapper -->
<?php $this->load->view('include/footer'); ?>
