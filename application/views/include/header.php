<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BMIS</title>
  <!-- Responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="<?=ASSETS?>favicon.ico">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=ASSETS?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/font-awesome.css">  
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/style.css">
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/dataTables.bootstrap.css">
  
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/skins/skin-black.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=ASSETS?>dist/css/custom.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <script>
function goBack(e) {
  //e.preventDefault();
  console.log(window.history);
    window.history.back();
    
}
</script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->
</head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="dashboard.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">BMIS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">BMIS</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">togglegle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?=($this->session->userdata('auth')['image']) ? ASSETS.'dist/img/'.$this->session->userdata('auth')['image'] : ASSETS.'dist/img/default.jpeg' ?>" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?=$this->session->userdata('auth')['username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                
                <p>
                <?=$this->session->userdata('auth')['username'];?>
                  <small></small>
                </p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=DOMAIN.'profile/add_edit_user/'.$this->session->userdata('auth')['id']?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                
                <div class="pull-right">
                  <a href="#" data-toggle="modal" data-target="#logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>

            </ul>
          </li>
          
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="image">
          
        </div>
        <div class="info">
          <p></p>          
        </div>
      </div>

      <!-- search form (Optional) -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- <li class="header">Dashboard</li> -->
        <!-- Optionally, you can add icons to the links -->
        <li class="<?php if($this->uri->segment(1)=='dashboard') { echo 'active'; } ?>"><a href="<?=DOMAIN?>dashboard"><i class="fa fa-dashboard"></i> <span>HOME</span></a></li>
        
        <li class="treeview <?php if($this->uri->segment(1)=='bmis') { echo 'active'; } ?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>BMIS</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=DOMAIN?>bmis/add_edit_bmis"><i class="fa fa-circle-o"></i>ADD BMIS</a></li>
            <li><a href="<?=DOMAIN?>bmis"><i class="fa fa-circle-o"></i> BMIS List</a></li>
            
          </ul>
        </li>

        
        
        <?php if($this->session->userdata('auth')['role_id']==1){ ?>
        <li class="treeview <?php if($this->uri->segment(1)=='profile') { echo 'active'; } ?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>AGENTS</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=DOMAIN?>profile/add_edit_user"><i class="fa fa-circle-o"></i>ADD Agent</a></li>
            <li><a href="<?=DOMAIN?>profile"><i class="fa fa-circle-o"></i> Agent List</a></li>
            
          </ul>
        </li>
        
        <li class="treeview <?php if($this->uri->segment(1)=='master') { echo 'active'; } ?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=DOMAIN?>master/country"><i class="fa fa-circle-o"></i>Country</a></li>
            <li><a href="<?=DOMAIN?>master/state"><i class="fa fa-circle-o"></i> State</a></li>
            <li><a href="<?=DOMAIN?>master/city"><i class="fa fa-circle-o"></i> City</a></li>
            <li><a href="<?=DOMAIN?>master/location"><i class="fa fa-circle-o"></i> Location</a></li>
            <li><a href="<?=DOMAIN?>master/cluster"><i class="fa fa-circle-o"></i>Cluster</a></li>
            <li><a href="<?=DOMAIN?>master/product"><i class="fa fa-circle-o"></i>Product</a></li>
            <li><a href="<?=DOMAIN?>master/vehicle_category"><i class="fa fa-circle-o"></i>Vehicle Category</a></li>
            <li><a href="<?=DOMAIN?>master/hpn"><i class="fa fa-circle-o"></i>HPN</a></li>
            <li><a href="<?=DOMAIN?>master/policy_type"><i class="fa fa-circle-o"></i>Policy Type</a></li>
            <li><a href="<?=DOMAIN?>master/ncb"><i class="fa fa-circle-o"></i>NCB</a></li>
            <li><a href="<?=DOMAIN?>master/verticle"><i class="fa fa-circle-o"></i>Verticle</a></li>
            <li><a href="<?=DOMAIN?>master/business_type"><i class="fa fa-circle-o"></i>Business Type</a></li>
            <li><a href="<?=DOMAIN?>master/sourcing_category"><i class="fa fa-circle-o"></i>Sourcing Category</a></li>
            <li><a href="<?=DOMAIN?>master/source_type"><i class="fa fa-circle-o"></i>Source Type</a></li>
            <li><a href="<?=DOMAIN?>master/insurance_company"><i class="fa fa-circle-o"></i>Insurance Company</a></li>
            <li><a href="<?=DOMAIN?>master/payment_mode"><i class="fa fa-circle-o"></i>Payment Received mode</a></li>
            <li><a href="<?=DOMAIN?>master/source_code_name"><i class="fa fa-circle-o"></i>Source Code-Name</a></li>
            <li><a href="<?=DOMAIN?>master/insurance_exec_code_name"><i class="fa fa-circle-o"></i>Insurance Executive Code-Name</a></li>
          </ul>
        </li>
        <?php } ?>
        <li class=""><a href="#" data-toggle="modal" data-target="#logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>