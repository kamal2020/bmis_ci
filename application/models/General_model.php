<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * Developer: kamal swami
 * Company : Coretechies
 */
class General_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function check_register_email_exists_or_not($email)
    {

        $this->db->from('users');
        $this->db->where('email_address', $email);
        $email_exists_query = $this->db->get();
        //echo $this->db->last_query();
        if ($email_exists_query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
     /**
     *  this function use for check email already exists in database for forgot password
     * @return query run
     */
    
    

    /**
     * this function use for get table full description
     * @param $table_name (get details database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (where condition variable value mandatory get details)
     * @return mixed (return data row array)
     */
    function get_full_description($table_name, $select_filds, $where_condition, $order = null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($order) {
            $this->db->order_by($order);
        }


        $this->db->from($table_name);
        $this->db->where($where_condition);
        $query = $this->db->get();
        $result = $query->row();
        //echo $this->db->last_query();
        return $result;
    }

    /**
     * this is simple insert data function
     * @param $table_name (database insert table name)
     * @param $insert_details (insert data array)
     * @return mixed (return true or false)
     */
    function insert_details($table_name, $insert_details)
    {
        $return_value = $this->db->insert($table_name, $insert_details);
        return $return_value;
    }

    /**
     * this function insert data and return last insert id
     * @param $table_name (database insert table name)
     * @param $insert_details (insert data array)
     * @return mixed (return last insert id)
     */
    function get_lastinsertid_after_insertdata($table_name, $insert_details)
    {
        $return_value = $this->db->insert($table_name, $insert_details);
        $last_insert_id = $this->db->insert_id();
        //echo $this->db->last_query();die;
        return $last_insert_id;
    }

    /**
     * this function use for insert batch details
     * @param $table_name
     * @param $insert_details
     * @return mixed
     */
    function insert_batch_details($table_name, $insert_details)
    {
        $return_value = $this->db->insert_batch($table_name, $insert_details);
        return $return_value;
    }

    /**
     * this function use for update data in database
     * @param $table_name (database update table name)
     * @param $update_data (update data array)
     * @param $where_condition (update condition array)
     * @param $orwhere_condition (update secound condition array like or where)
     * @return mixed (return update successfully YES or NOT)
     */
    function update_details($table_name, $update_data, $where_condition = null, $orwhere_condition = null)
    {
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($orwhere_condition != '') {
            $this->db->or_where($orwhere_condition);
        }
        $return_data = $this->db->update($table_name, $update_data);
         //echo $this->db->last_query();die;
        return $return_data;
    }

    /**
     * this function use for get listing data from database
     * @param $table_name (get listing database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (get data where condition if send black then no condition apply all details fetch else condition base data fetch)
     * @return mixed (return get data array)
     */
    function get_listing_details($table_name, $select_filds, $where_condition, $order_by = null,$limit=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if($limit)
        {
            $this->db->limit($limit);
        }
        $query = $this->db->get($table_name);
        $result = $query->result();
         //echo $this->db->last_query();
        return $result;
    }

    function get_listing_where_in($table_name, $select_filds,$match_field, $where_condition, $order_by = null)
    {   //var_dump($where_condition);
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where_in($match_field,$where_condition);
        }

        if ($order_by) {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get($table_name);
        $result = $query->result();
        // echo $this->db->last_query();exit;
        return $result;
    }

   
    /**
     * this function use for get listing data from database
     * @param $table_name (get listing database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (get data where condition if send black then no condition apply all details fetch else condition base data fetch)
     * @return mixed (return get data array)
     */
    function get_notification_listing_details($table_name, $select_filds, $where_condition)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        $this->db->where('response_customer_id', $this->session->userdata('customerLoginDetails')['customer_id'], 'notification_deleted', 'N');
        $query = $this->db->get($table_name);
        $result = $query->result();
        return $result;
    }

    
 function get_listing_details_by_limit($table_name, $select_filds, $where_condition, $order_by, $limit, $start)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }

        $this->db->order_by($order_by, 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get($table_name);
        $result = $query->result();
        return $result;
    }

  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * this function use for get all country list
     * @return mixed this function return all countries  array
     */

    public function get_country_state_form_city_id($city_id = null)
    {
        $this->db->select('master_city.city_name,master_state.state_name,master_country.country_name');
        $this->db->from('master_city');
        $this->db->join('master_state', 'master_state.state_id = master_city.state_id');
        $this->db->join('master_country', 'master_country.country_id = master_state.country_id');
        $this->db->where('master_city.city_id', $city_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function get_courntry_listing_details()
    {
        $this->db->select('id,name');
        $this->db->from('countries');
        $this->db->where('deleted', 'N');
        $this->db->order_by('name', 'ASC');
        $this->db->group_by('name');
        $query_data = $this->db->get();
        $country_data = $query_data->result();
        return $country_data;
    }

    /**
     * this function use for get state listing details using by country id
     * @param $country_id (pass your select country )
     * @return mixed this function return state listing array
     */
    function get_state_listing_details($country_id)
    {
        $this->db->select('id,name');
        $this->db->from('states');
        $this->db->where('deleted', 'N');
        $this->db->where('country_id', $country_id);
        $this->db->order_by('name', 'ASC');
        $this->db->group_by('name');
        $query_data = $this->db->get();
        $state_data = $query_data->result();
        return $state_data;
    }

    /**
     *  this function use for get city listing details using by state id
     * @param $state_id first select your state then load all city base on state
     * @return mixed this function return city listing details
     */
    function get_city_listing_details($state_id)
    {
        $this->db->select('id,name');
        $this->db->from('cities');
        $this->db->where('deleted', 'N');
        $this->db->where('state_id', $state_id);
        $this->db->order_by('name', 'ASC');
        $this->db->group_by('name');
        $query_data = $this->db->get();
        $city_data = $query_data->result();
        return $city_data;
    }
    function get_multipel_data($table,$field,$check,$selected)
    {
        $selected= json_decode($selected);
        $this->db->select($field);
        $this->db->where_in($check,$selected);
        $query = $this->db->get($table);
        $data = array();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return '';
    }

    /**
     * this function use for remove data from database just pass some value and this is global function
     * @param $table_name remove data table name
     * @param $where_condition remove where condition
     * @return mixed return remove success or not
     */
    function remove_details_from_database($table_name, $where_condition)
    {
        $return_details = $this->db->delete($table_name, $where_condition);
        return $return_details;
    }

    function remove_profile_image_from_database($table_name, $where_condition)
    {

        $this->db->select('profile_image');
        $this->db->from($table_name);
        $this->db->where($where_condition);
        $query = $this->db->get();
        $result = $query->row();
        if ($result) {
            $Path = "upload_dir/member_images/";
            $old_File = $Path . $result->profile_image;
            unlink($old_File);
        }

        $return_details = $this->db->delete($table_name, $where_condition);
        return $return_details;
    }
    

    function deleteDetailFromDatabase($table_name, $where_condition)
    {
        $return_details = $this->db->delete($table_name, $where_condition);
        return $return_details;
    }

    
   //========Update Data in Database===================//
    function update_details_where_in($table_name, $update_data,$field, $where_condition)
    {
        //===check where condition===//
        if ($where_condition != '') {
            $this->db->where_in($field,$where_condition);
        }
        //=====check orwhere condition=//

        //=====update query=====//
        $return_data = $this->db->update($table_name, $update_data);
        //echo $this->db->last_query();exit;
        return $return_data;
    }
    
    /**
     * this function use for total count record
     * @param $table_name count from table name
     * @param $where_condition count condition
     * @return mixed return total record
     */
    function get_total_count_record($table_name, $where_condition)
    {
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        $this->db->from($table_name);
        $total_result = $this->db->count_all_results();
        //echo $this->db->last_query();die;
        return $total_result;
    }

   

    /**
     * this function use for get listing data from database
     * @param $table_name (get listing database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (get data where condition if send black then no condition apply all details fetch else condition base data fetch)
     * @return mixed (return get data array)
     */
    function get_listing_details_limit($table_name, $select_filds, $where_condition, $limit)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        $this->db->limit($limit);

        $query = $this->db->get($table_name);
        $result = $query->result();

        return $result;
    }
    
    function get_booked_session($start, $end, $vander_id, $appointment_date)
    {
        $sql = "SELECT `session_start_time`,`session_end_time` FROM `profile_astro_booking_appointment` WHERE
           `appointment_date` = '$appointment_date' and `vendor_id` ='$vander_id' and `session_start_time` BETWEEN '$start' and '$end'";
        $query = $this->db->query($sql);
        // echo $this->db->last_query();exit;
        $result = $query->result();
        return $result;

    }


    public function insertCSV($data)
    {
        $this->db->insert('country_currency_info', $data);
        return $this->db->insert_id();
    }

   
    
    function get_detail_group_by($table, $field, $where, $id)
    {
        if ($field != '') {
            $this->db->select($field);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table);
        $this->db->where($where);
        $this->db->group_by($id);
        $query_data = $this->db->get();
        $data = $query_data->result();
        return $data;
    }
    
   
    function get_listing_details_limit_start($table_name, $select_filds, $where_condition, $order_by, $limit, $start)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        $this->db->order_by($order_by, 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get($table_name);
        $result = $query->result();
        return $result;
    }

 
}
