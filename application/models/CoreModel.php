<?php
class CoreModel extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function setSession($data){
		unset($data[0]->passCode,$data[0]->status);
		$this->session->set_userdata('AgentloginTokn',uniqid());
		$this->session->set_userdata('auth',(array)$data[0]);
    }
    function get_total_count_record($table_name, $where_condition=null,$where_condition2=null)
    {
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($where_condition2 != '') {
            $this->db->where($where_condition2);
        }
        $this->db->from($table_name);
        $total_result = $this->db->count_all_results();
        
        //echo $this->db->last_query();die;
        return $total_result;
    }
    
    function get_full_description($table_name, $select_filds, $where_condition=null, $order = null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($order) {
            $this->db->order_by($order);
        }


        $this->db->from($table_name);
        if($where_condition){
        $this->db->where($where_condition);
        }
        $query = $this->db->get();
        $result = $query->row();
        //echo $this->db->last_query();
        return $result;
    }
    function insert_details($table_name, $insert_details)
    {
        $return_value = $this->db->insert($table_name, $insert_details);
        //echo $this->db->last_query();
        return $return_value;
    }

    function get_lastinsertid_after_insertdata($table_name, $insert_details)
    {
        $return_value = $this->db->insert($table_name, $insert_details);
        $last_insert_id = $this->db->insert_id();
        //echo $this->db->last_query();die;
        return $last_insert_id;
    }
    function update_details($table_name, $update_data, $where_condition = null, $orwhere_condition = null)
    {
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($orwhere_condition != '') {
            $this->db->or_where($orwhere_condition);
        }
        $return_data = $this->db->update($table_name, $update_data);
         //echo $this->db->last_query();die;
        return $return_data;
    }
    function get_listing_details($table_name, $select_filds, $where_condition, $order_by = null,$limit=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if($limit)
        {
            $this->db->limit($limit);
        }
        $query = $this->db->get($table_name);
        $result = $query->result();
         //echo $this->db->last_query();
        return $result;
    }
    function get_bmis_listing_details($select = null, $range = null)
    {
        //var_dump($select);
        if($select)
        {
            if ($range)
            {
                if($this->session->userdata('auth')['role_id']!=1){
                    $user_id=$this->session->userdata('auth')['id'];
                $where=" where bmis.user_id='$user_id' and bmis.entry_date between '$range[0]' and '$range[1]' ";
                }else{ 
                $where=" where bmis.entry_date between '$range[0]' and '$range[1]' ";}
            }
            else {
                if($this->session->userdata('auth')['role_id']!=1){
                    $user_id=$this->session->userdata('auth')['id'];
                $where=" where bmis.user_id='$user_id'";
                }else{ 
                $where="";}
            }
            
            $query=$this->db->query("select  $select from bmis left join user on user.id=bmis.user_id $where order by bmis.id desc");
            
            //echo $this -> db -> last_query();die;
            $filename = 'bmis_'.date('Y-m-d').'.csv'; 
	   header("Content-Description: File Transfer"); 
	   header("Content-Disposition: attachment; filename=$filename"); 
	   header("Content-Type: application/csv; ");
	   
	   // get data 
       $usersData = $query->result_array();
       
     // $array = get_object_vars($usersData); 
      $properties = array_keys($usersData[0]);

	   // file creation 
	   $file = fopen('php://output', 'w');
	 
	  $header = $properties; 
	   fputcsv($file, $header);
	   foreach ($usersData as $key=>$line){ 
        //var_dump($line);die;
        if (isset($line['Insurance Company']) && isset($line['Policy Number']))
        {
            if($line['Insurance Company'] == 'Liberty Videocon General Insurance Co. Ltd.')
            {
                $line['Policy Number']='L'.$line['Policy Number'];
            }
            if($line['Insurance Company'] == 'Reliance General Insurance Co. Ltd.')
            {
                $line['Policy Number']='R'.$line['Policy Number'];
            }
            if($line['Insurance Company'] == 'Hdfc Ergo General Insurance Co. Ltd.' || $line['Insurance Company'] == 'Hdfc Standard Life Insurance Co. Ltd.')
            {
                $line['Policy Number']='H'.$line['Policy Number'];
            }
            if($line['Insurance Company'] == 'National Insurance Co.Ltd.')
            {
                $line['Policy Number']='N'.$line['Policy Number'];
            }
            if($line['Insurance Company'] == 'The New India Assurance Co. Ltd.')
            {
                $line['Policy Number']='N'.$line['Policy Number'];
            }
        }       
        fputcsv($file,$line); 
	   }
	   fclose($file); 
	   exit; 
        }

        else{
            $this->db->select('bmis.*,user.username');
        }

        $this->db->join('user', 'user.id = bmis.user_id');
        if ($range)
        {
            $this -> db -> where("bmis.entry_date >=",$range[0]);
            $this -> db -> where("bmis.entry_date <=",$range[1]);
        }
        if($this->session->userdata('auth')['role_id']!=1)
        {
          $this->db->where('bmis.user_id',$this->session->userdata('auth')['id']);  
        }
        $this->db->order_by('bmis.id','desc');
        $query = $this->db->get('bmis');
        $result = $query->result();
        if($select){
       
        }
        else {
            return $result; 
        }

        
    }
    function get_state_listing_details()
    {
        $this->db->select('state.*,countries.country_name');
        //$this->db->where($where_condition);
        $this->db->join('countries', 'countries.id = state.country_id');
        $query = $this->db->get('state');
        $result = $query->result();
         //echo $this->db->last_query();
        return $result;
    }
    function get_city_listing_details()
    {
        $this->db->select('city.*,state.state_name');
        //$this->db->where($where_condition);
        $this->db->join('state', 'state.id = city.state_id');
        $query = $this->db->get('city');
        $result = $query->result();
         //echo $this->db->last_query();
        return $result;
    }
    function get_cluster_listing_details()
    {
        $this->db->select('cluster.*,state.state_name');
        //$this->db->where($where_condition);
        $this->db->join('state', 'state.id = cluster.state_id');
        $query = $this->db->get('cluster');
        $result = $query->result();
         //echo $this->db->last_query();
        return $result;
    }
    function get_vehicle_category_listing_details()
    {
        $this->db->select('vehicle_category.*,product_category.product');
        $this->db->join('product_category', 'product_category.id = vehicle_category.product_id');
        $query = $this->db->get('vehicle_category');
        $result = $query->result();
        return $result;

    }
    function get_business_type_listing_details()
    {
        $this->db->select('business_type.*,verticle.verticle');
        $this->db->join('verticle', 'verticle.id = business_type.verticle_id');
        $query = $this->db->get('business_type');
        $result = $query->result();
        return $result;

    }
    function get_source_type_listing_details()
    {
        $this->db->select('source_type.*,sourcing_category.sourcing_category');
        $this->db->join('sourcing_category', 'sourcing_category.id = source_type.sourcing_category_id');
        $query = $this->db->get('source_type');
        $result = $query->result();
        return $result;

    }
    
    function get_listing_details_by_limit($table_name, $select_filds, $where_condition, $order_by, $limit, $start)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }

        $this->db->order_by($order_by, 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get($table_name);
        $result = $query->result();
        return $result;
    }
	
	function checkEmail(){
        return $this->db->select('*')
        			->where('emailAddress',$this->session->userdata('auth')['emailAddress'])
        			->where('status',1)
        			->get('credentials')->result();
    }
    
    function getUserById($id){
        return $this->db->select('*')->where('id',$id)->get('credentials')->result();
    }
    function setPassword(){
    	return $this->db->update('credentials',array('passCode' => password_hash($this->input->post('nPassword'),PASSWORD_DEFAULT)), "id =".$this->session->userdata('auth')['id']);
    }
    
    function fileUpload(){
		$config['upload_path']          = '../assets/dist/img/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx';
        $config['max_size']             = 9999;
        $config['encrypt_name'] 		= TRUE;
        $this->load->library('upload', $config);
       	if($this->upload->do_upload('file'))
       		return $this->upload->data('file_name');
       	else
       		echo $this->upload->display_errors();die;

    }
    
    function get_product($id) 
    {
        $this -> db -> select('product_category.product as pro');
        $this -> db -> join('product_category', 'vehicle_category.product_id = product_category.id', 'left');
        $this -> db -> where('vehicle_category.id', $id);
        return $this -> db -> get('vehicle_category') -> row();
    }

    function get_verticle($id) 
    {
        $this -> db -> select('verticle.verticle as verticle');
        $this -> db -> join('verticle', 'business_type.verticle_id = verticle.id', 'left');
        $this -> db -> where('business_type.id', $id);
        return $this -> db -> get('business_type') -> row();
    }

    function get_sourcing_category($id) 
    {
        $this -> db -> select('sourcing_category.sourcing_category as category');
        $this -> db -> join('sourcing_category', 'source_type.sourcing_category_id = sourcing_category.id', 'left');
        $this -> db -> where('source_type.id', $id);
        return $this -> db -> get('source_type') -> row();
    }

    function get_source_name($id) 
    {
        $this -> db -> select('source_code_source_name.source_name as name');
        $this -> db -> where('source_code_source_name.source_code', $id);
        return $this -> db -> get('source_code_source_name') -> row();
    }

    function get_exec_name($id) 
    {
        $this -> db -> select('insurance_exec_code.insurance_exec_name as name');
        $this -> db -> where('insurance_exec_code.insurance_exec_code', $id);
        return $this -> db -> get('insurance_exec_code') -> row();
    }
    
}