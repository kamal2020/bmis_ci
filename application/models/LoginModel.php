<?php
class LoginModel extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function checkEmail(){
        return $this->db->select('*')
        			->where('email',$this->input->post('email'))
        			->where('status',1)
        			->get('user')->result();
    }
}